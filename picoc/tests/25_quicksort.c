#include <stdio.h>

int int_array[16];

//Swap integer values by array indexes
void swap(int a, int b)
{
    int tmp  = int_array[a];
    int_array[a] = int_array[b];
    int_array[b] = tmp;
}

//Partition the array into two halves and return the
//index about which the array is partitioned
int partition(int left, int right)
{
    int pivotIndex = left;
    int pivotValue = int_array[pivotIndex];
    int index = left;
    int i;
 
    swap(pivotIndex, right);
    for(i = left; i < right; i++)
    {
        if(int_array[i] < pivotValue)
        {
            swap(i, index);
            index += 1;
        }
    }
    swap(right, index);
 
    return index;
}
 
//Quicksort the array
void quicksort(int left, int right)
{
    if(left >= right)
        return;
 
    int index = partition(left, right);
    quicksort(left, index - 1);
    quicksort(index + 1, right);
}
 
void main()
{
    int i;

    int_array[0] = 62;
    int_array[1] = 83;
    int_array[2] = 4;
    int_array[3] = 89;
    int_array[4] = 36;
    int_array[5] = 21;
    int_array[6] = 74;
    int_array[7] = 37;
    int_array[8] = 65;
    int_array[9] = 33;
    int_array[10] = 96;
    int_array[11] = 38;
    int_array[12] = 53;
    int_array[13] = 16;
    int_array[14] = 74;
    int_array[15] = 55;

    for (i = 0; i < 16; i++)
        printf("%d ", int_array[i]);

    printf("\n");

    quicksort(0, 15);

    for (i = 0; i < 16; i++)
        printf("%d ", int_array[i]);

    printf("\n");
}

