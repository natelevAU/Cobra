/* used for PHP arrays */
#include "../interpreter.h"

#ifndef BUILTIN_MINI_STDLIB


#include "bin_tree.h"
#include "array.h"

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define MEM_ALIGN(x) (((x) + sizeof(ALIGN_TYPE) - 1) & ~(sizeof(ALIGN_TYPE)-1))




struct Value *shiftArg(struct Value *arg) {
    arg = (struct Value *)((void*)arg + MEM_ALIGN(sizeof(struct Value) + TypeStackSizeValue(arg)));
    return arg;
}

struct Value *shiftArgBy(struct Value *arg, int i) {
    for (; i; i--)
        arg = shiftArg(arg);
    return arg;
}

int isPos(int num) {
    return num >= 0;
}



Mixed helperStdArrayNodeGet(struct Value **currArgP, int NumArgs, int initialise)
{
    struct Value *currArg = (*currArgP);
    Array array = currArg->Val->Pointer;
    ArrayNode arrayNode;
    Key key;
    int keyType;

    for (int i = 1; i < NumArgs; i += 2) {
        currArg = shiftArg(currArg);
        key = currArg->Val->Pointer;
        currArg = shiftArg(currArg);
        keyType = currArg->Val->Pointer;

        keyType = calcKeyType(key, keyType);

        arrayNode = getArrayNodeByKey(array, key, keyType, initialise);

        if (arrayNode != NULL && calcIsArray(arrayNode->element, arrayNode->elementType)) {
            array = arrayNode->element;
        }
    }

    *currArgP = currArg;
    return arrayNode;
}



void StdArrayGet(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    ArrayNode arrayNode = helperStdArrayNodeGet(&currArg, NumArgs, FALSE);

    if (isNum(arrayNode->element, arrayNode->elementType))
        ReturnValue->Typ->BaseDataType = arrayNode->elementType;

    ReturnValue->Val->Pointer = arrayNode->element;
}

void StdArraySet(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    ArrayNode arrayNode = helperStdArrayNodeGet(&currArg, NumArgs - 2, TRUE);

    currArg = shiftArg(currArg);
    Mixed element = currArg->Val->Pointer;
    currArg = shiftArg(currArg);
    int elementType = currArg->Val->Pointer;

    elementType = calcMixedType(element, elementType);
    
    setArrayNodeElement(arrayNode, element, elementType);
}







// void array_print(Array array)
void StdArrayPrint(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array array = currArg->Val->Pointer;
    
    arrayPrint(array);
    printf("\n");
}

// Array array_fill(int start_index, int count, Mixed value)
void StdArrayFill(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    int start_index = Param[0]->Val->Integer;
    int count = Param[1]->Val->Integer;
    Mixed value = Param[2]->Val->Pointer;
    int valueType = Param[3]->Val->Integer;

    Array array = newArray();
    for (int i = 0; i < count; i++) {
        addArrayNodeEnd(array, newArrayNode(start_index + i, DataTypeInt, value, valueType));
        if (start_index < 0)
            start_index = -1;
    }

    ReturnValue->Val->Pointer = array;
    ReturnValue->Typ->BaseDataType = DataTypeArray;
}

// Array array_flip(Array array)
void StdArrayFlip(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array oldArray = Param[0]->Val->Pointer;
    Array array = newArray();

    if (oldArray == NULL || oldArray->size == 0)
        return;

    for (ArrayNode currNode = oldArray->head; currNode != NULL; currNode = currNode->next) {
        if (isKey(currNode->element, currNode->elementType)) {
            addArrayNodeEnd(array, newArrayNode(currNode->element, currNode->elementType, currNode->key, currNode->keyType));
        }
    }

    ReturnValue->Val->Pointer = array;
    ReturnValue->Typ->BaseDataType = DataTypeArray;
}

// int array_key_exists(Key key, Array array)
void StdArrayKeyExists(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Key key = currArg->Val->Pointer;
    currArg = shiftArg(currArg);
    int keyType = currArg->Val->Pointer;
    currArg = shiftArg(currArg);
    Array array = currArg->Val->Pointer;

    keyType = calcKeyType(key, keyType);

    ReturnValue->Val->Integer = (getArrayNodeByKey(array, key, keyType, FALSE) != NULL);
    ReturnValue->Typ->BaseDataType = DataTypeInt;
}

// Key array_key_first(Array array)
void StdArrayKeyFirst(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Pointer = NULL;
    struct Value *currArg = Param[0];
    Array array = Param[0]->Val->Pointer;

    if (array != NULL) {
        if (array->head != NULL) {
            ReturnValue->Val->Pointer = array->head->key;
            ReturnValue->Typ->BaseDataType = array->head->keyType;
        } else {
            ReturnValue->Val->Pointer = NULL;
            ReturnValue->Typ->BaseDataType = 0;
        }
    }
}

// Key array_key_last(Array array)
void StdArrayKeyLast(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Pointer = NULL;
    struct Value *currArg = Param[0];
    Array array = Param[0]->Val->Pointer;

    if (array != NULL) {
        if (array->head != NULL) {
            ReturnValue->Val->Pointer = array->tail->key;
            ReturnValue->Typ->BaseDataType = array->tail->keyType;
        } else {
            ReturnValue->Val->Pointer = NULL;
            ReturnValue->Typ->BaseDataType = DataTypePointer;
        }
    }
}

// Array array_keys(Array array)
// Array array_keys(Array array, Mixed search_value)
// Array array_keys(Array array, Mixed search_value, bool strict)
void StdArrayKeys(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array oldArray = currArg->Val->Pointer;
    Mixed search_value = NULL;
    int search_valueType = NULL;
    int strict = FALSE;

    if (NumArgs > 2) {
        currArg = shiftArg(currArg);
        search_value = currArg->Val->Pointer;
        currArg = shiftArg(currArg);
        search_valueType = currArg->Val->Pointer;
        if (NumArgs > 3) {
            currArg = shiftArg(currArg);
            strict = currArg->Val->Pointer;
        }
    }

    Array array = newArray();
    ReturnValue->Val->Pointer = array;
    ReturnValue->Typ->BaseDataType = DataTypeArray;

    if (oldArray == NULL || oldArray->size == 0)
        return;

    for (ArrayNode currNode = oldArray->head; currNode != NULL; currNode = currNode->next) {
        if (search_value || search_valueType) {
            if (!elementComparable(currNode, search_value, search_valueType) || elementCmp(currNode, search_value, search_valueType))
                continue;
        }
        addArrayNode(array, newArrayNode(NULL, 0, currNode->key, currNode->keyType));
    }
}

// Mixed array_pop(Array array)
void StdArrayPop(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array array = Param[0]->Val->Pointer;
    ArrayNode arrayNode = removeArrayNodeEnd(array);

    ReturnValue->Val->Pointer = arrayNode->element;
    ReturnValue->Typ->BaseDataType = arrayNode->elementType;
}

// int array_push(Array array, Mixed ...values)
void StdArrayPush(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array array = currArg->Val->Pointer;

    Mixed element;
    int elementType;
    int newElements = 0;
    for (int i = 0; (i + 1) < (NumArgs - 1); i+= 2) {
        currArg = shiftArg(currArg);
        element = currArg->Val->Pointer;
        currArg = shiftArg(currArg);
        elementType = currArg->Val->Integer;

        addArrayNode(array, newArrayNode(NULL, NULL, element, elementType));
        newElements++;
    }

    ReturnValue->Val->Integer = newElements;
    ReturnValue->Typ->BaseDataType = DataTypeInt;
}

// Key array_search(Mixed needle, Array haystack)
// Key array_search(Mixed needle, Array haystack, bool strict)
void StdArraySearch(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Mixed needle = currArg->Val->Pointer;
    currArg = shiftArg(currArg);
    int needleType = currArg->Val->Integer;
    currArg = shiftArg(currArg);
    Array haystack = currArg->Val->Pointer;
    int strict = FALSE;
    if (NumArgs >= 4) {
        currArg = shiftArg(currArg);
        strict = currArg->Val->Pointer;
    }

    // TODO Implement strict

    ArrayNode arrayNode = getArrayNodeByElement(haystack, needle, needleType, FALSE);

    if (arrayNode == NULL) {
        ReturnValue->Val->Pointer = FALSE;
        ReturnValue->Typ->BaseDataType = DataTypeInt;
    } else {
        ReturnValue->Val->Pointer = arrayNode->key;
        ReturnValue->Typ->BaseDataType = arrayNode->keyType;
    }
}

// Mixed array_shift(Array array)
void StdArrayShift(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array array = Param[0]->Val->Pointer;

    ArrayNode removedNode = removeArrayNodeStart(array);
    ReturnValue->Val->Pointer = removedNode->element;
    ReturnValue->Typ->BaseDataType = removedNode->elementType;

    int i = 0;
    for (ArrayNode currNode = array->head; currNode != NULL; currNode = currNode->next) {
        if (isNum(currNode->key, currNode->keyType)) {
            setArrayNodeKey(currNode, i, DataTypeInt);
            i++;
        }
    }
}

// Array array_slice(Array array, int offset)
// Array array_slice(Array array, int offset, int length)
// Array array_slice(Array array, int offset, int length, bool preserve_keys)
void StdArraySlice(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array oldArray = currArg->Val->Pointer;
    currArg = shiftArg(currArg);
    int offset = currArg->Val->Integer;
    int length = oldArray->size - offset;
    int preserve_keys = FALSE;

    if (NumArgs >= 3) {
        currArg = shiftArg(currArg);
        length = currArg->Val->Integer;
        if (NumArgs >= 4) {
            currArg = shiftArg(currArg);
            preserve_keys = (currArg->Val->Integer) ? 1 : 0;
        }
    }

    if (length > oldArray->size - offset) {
        length = oldArray->size - offset;
    } else
    if (length < offset - oldArray->size) {
        length = oldArray->size - offset;
    }

    Array array = newArray();
    ReturnValue->Val->Pointer = array;
    ReturnValue->Typ->BaseDataType = DataTypeArray;

    if (offset >= oldArray->size || -offset > oldArray->size)
        return;
    
    ArrayNode currNode = getArrayNodeByIndex(oldArray, offset);

    for (int i = 0; i < length && currNode != NULL; i++) {
        addArrayNode(array, copyArrayNode(currNode, preserve_keys));
        currNode = isPos(offset) ? currNode->next : currNode->prev;
    }
}

// Num array_sum(Array array)
void StdArraySum(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array array = Param[0]->Val->Pointer;
    int sum = 0;

    for (ArrayNode currNode = array->head; currNode != NULL; currNode = currNode->next) {
        if (isNum(currNode->element, currNode->elementType)) {
            sum += (int) currNode->element;
        }
    }

    ReturnValue->Val->Integer = sum;
    ReturnValue->Typ->BaseDataType = DataTypeInt;
}

// int array_unshift(Array array, Mixed ...values)
void StdArrayUnshift(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Integer = 0;
    
    struct Value *currArg = Param[0];
    Array array = currArg->Val->Pointer;
    if (array == NULL)
        return;

    int numNewElements = (NumArgs - 1) / 2;
    if (numNewElements == 0)
        return;
    for (ArrayNode currNode = array->head; currNode != NULL; currNode = currNode->next) {
        if (isNum(currNode->key, currNode->keyType)) {
            currNode->key += numNewElements;
        }
    }

    ArrayNode currNode = NULL;
    for (int i = 0; i < numNewElements; i++) {
        currArg = shiftArg(currArg);
        Mixed element = currArg->Val->Pointer;
        currArg = shiftArg(currArg);
        int elementType = currArg->Val->Pointer;
        currNode = addArrayNodeAfter(array, newArrayNode(i, DataTypeInt, element, elementType), currNode);
    }
    
    ReturnValue->Val->Integer = array->size;
    ReturnValue->Typ->BaseDataType = DataTypeInt;
}

// Array array_values(Array array)
void StdArrayValues(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array oldArray = Param[0]->Val->Pointer;
    Array array = newArray();

    ReturnValue->Val->Pointer = array;
    ReturnValue->Typ->BaseDataType = DataTypeArray;

    if (oldArray == NULL || oldArray->size == 0)
        return;

    for (ArrayNode currNode = oldArray->head; currNode != NULL; currNode = currNode->next) {
        addArrayNode(array, newArrayNode(NULL, NULL, currNode->element, currNode->elementType));
    }
    printf("\n");
}

// int count(Countable|Array array)
// int count(Countable|Array array, int mode)
void StdCount(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array array = currArg->Val->Pointer;
    int mode = COUNT_NORMAL;
    if (NumArgs >= 2) {
        currArg = shiftArg(currArg);
        mode = currArg->Val->Integer;
    }

    ReturnValue->Val->Integer = arrayCount(array, mode);
    ReturnValue->Typ->BaseDataType = DataTypeInt;
}

// Mixed end(Array|Object array)
void StdEnd(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    Array array = Param[0]->Val->Pointer;

    ReturnValue->Val->Pointer = setArrayPointer(array, array->tail);
    if (array != NULL)
        ReturnValue->Typ->BaseDataType = array->tail->elementType;
}

// Bool in_array(Mixed needle, Array haystack)
// Bool in_array(Mixed needle, Array haystack, Bool strict)
void StdInArray(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Mixed needle = currArg->Val->Pointer;
    currArg = shiftArg(currArg);
    int needleType = currArg->Val->Integer;
    currArg = shiftArg(currArg);
    Array haystack = currArg->Val->Pointer;
    int strict = FALSE;
    if (NumArgs >= 4) {
        currArg = shiftArg(currArg);
        strict = currArg->Val->Pointer;
    }

    // TODO Implement strict

    ArrayNode arrayNode = getArrayNodeByElement(haystack, needle, needleType, FALSE);
    ReturnValue->Val->Integer = (arrayNode != NULL);
    ReturnValue->Typ->BaseDataType = DataTypeInt;
}

// Array range(Key start, Key end)
// Array range(Key start, Key end, Num step)
void StdRange(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    int start = currArg->Val->Integer;
    currArg = shiftArg(currArg);
    int end = currArg->Val->Integer;
    currArg = shiftArg(currArg);
    int keyType = currArg->Val->Integer;
    int step = 1;
    if (NumArgs >= 4) {
        currArg = shiftArg(currArg);
        step = currArg->Val->Integer;
        if (step <= 0) // step must be a positive number - otherwise, default to 1
            step = 1;
    }

    Array array = newArray();
    for (int i = start; i <= end; i+= step) {
        addArrayNode(array, newArrayNode(NULL, NULL, i, keyType));
    }

    ReturnValue->Val->Pointer = array;
    ReturnValue->Typ->BaseDataType = DataTypeArray;
}

// Mixed reset(Array|Object array)
void StdReset(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array array = Param[0]->Val->Pointer;

    ReturnValue->Val->Pointer = setArrayPointerFirst(array);
    if (array != NULL)
        ReturnValue->Typ->BaseDataType = array->head->elementType;
}

// int count(Countable|Array array)
// int count(Countable|Array array, int mode)
void StdSizeOf(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    StdCount(Parser, ReturnValue, Param, NumArgs);
}

// Array aSort(Array array)
// Array aSort(Array array, Bool by_val)
// Array aSort(Array array, Bool by_val, Bool direction_up)
void StdAsort(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{   
    struct Value *currArg = Param[0];
    Array array = currArg->Val->Pointer;
    int by_val = TRUE;
    int direction_up = TRUE;
    if (NumArgs >= 2) {
        currArg = shiftArg(currArg);
        by_val = currArg->Val->Integer;
    }
    if (NumArgs >= 3) {
        currArg = shiftArg(currArg);
        direction_up = currArg->Val->Integer;
    }

    ReturnValue->Val->Integer = arraySort(array, by_val, direction_up);
    ReturnValue->Typ->BaseDataType = DataTypeInt;
}



void StdArray(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    Array array = newArray();
    ReturnValue->Val->Pointer = array;

    if (NumArgs == 0)
        return;
    
    
    struct Value *currArg = Param[0];
    Key arrayKey;
    int arrayKeyType;
    Mixed arrayElement;
    int arrayElementType;

    for (int i = 0; i < NumArgs - 3; i += 4) {
        arrayKey = currArg->Val->Pointer;
        currArg = shiftArg(currArg);
        arrayKeyType = currArg->Val->Pointer;
        currArg = shiftArg(currArg);
        arrayElement = currArg->Val->Pointer;
        currArg = shiftArg(currArg);
        arrayElementType = currArg->Val->Pointer;
        currArg = shiftArg(currArg);

        addArrayNode(array, newArrayNode(arrayKey, arrayKeyType, arrayElement, arrayElementType));
    }
}

void StdArrayDelete(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    struct Value *currArg = Param[0];
    Array array = currArg->Val->Pointer;

    deleteArray(array);
}




/* all array.h functions */
struct LibraryFunction StdArrayFunctions[] =
{
    { StdArrayPrint,    "void array_print(void *);" },
    { StdArrayFill,     "void *array_fill(int, int, void *, int);" },
    { StdArrayFlip,     "void *array_flip(void *);" },
    { StdArrayKeyExists,"int array_key_exists(void *, ...);" },
    { StdArrayKeyFirst, "void *array_key_first(void *);" },
    { StdArrayKeyLast,  "void *array_key_last(void *);" },
    { StdArrayKeys,     "void *array_keys(void *, ...);" },
    { StdArrayPop,      "void *array_pop(void *);" },
    { StdArrayPush,     "int array_push(void *, void *, ...);" },
    { StdArraySearch,   "void *array_search(void *, int, void *, ...);" },
    { StdArrayShift,    "void *array_shift(void *);" },
    { StdArraySlice,    "void *array_slice(void *, int, ...);" },
    { StdArraySum,      "int array_sum(void *);" },
    { StdArrayUnshift,  "int array_unshift(void *, ...);" },
    { StdArrayValues,   "void *array_values(void *);" },
    { StdCount,         "int count(void *, int);" },
    { StdEnd,           "void *end(void *);" },
    { StdInArray,       "int in_array(void *, int, void *);" },
    { StdRange,         "void *range(int, int, ...);" },
    { StdReset,         "void *reset(void *);" },
    { StdSizeOf,        "int size_of(void *, int);" },
    { StdAsort,         "int asort(void *, ...);" },

    { StdArray,         "void *array(void);" },
    { StdArray,         "void *arrayArgs(void *, ...);" },
    { StdArrayDelete,   "void arrayDelete(void *);" },

    { StdArrayGet,"void *arrayGet(void *, ...);" },
    { StdArraySet,"void arraySet(void *, ...);" },
    
    { NULL,             NULL }
};

#endif /* !BUILTIN_MINI_STDLIB */
