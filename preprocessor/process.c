#include "process.h"
#include "preprocess.h"
#include "token.h"
#include "array.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define ARRAY_GET "arrayGet"
#define ARRAY_SET "arraySet"

#define MAX_VARS_IN_FUNC 100


void processTokenList(TokenList tokenList) {
    if (tokenList == NULL || tokenList->head == NULL) return;
    TokenNode currNode;
    TokenNode prevNode = NULL;
    char *arrayList[MAX_STRING_LEN];
    arrayList[0] = NULL;
    for (currNode = tokenList->head; currNode != NULL; prevNode = currNode, currNode = currNode->next) {
        // printf("%s%s", currNode->token->preWhitespace != NULL ? currNode->token->preWhitespace : "", currNode->token->value);
        processToken(tokenList, arrayList, prevNode, currNode);
    }
    for (int i = 0; arrayList[i] != NULL; i++) {
        free(arrayList[i]);
    }
}

void processToken(TokenList tokenList, char **arrayList, TokenNode prevNode, TokenNode currNode) {
    if (prevNode == NULL)
        return;

    TokenType tokenType = currNode->token->type;
    if (tokenType == TokenIdentifier) {
        processTokenIdentifier(tokenList, arrayList, prevNode, currNode);
    } else
    if (tokenType == TokenPunctuator) {
        processTokenPunctuator(tokenList, arrayList, prevNode, currNode);
    }
}



void processTokenIdentifier(TokenList tokenList, char **arrayList, TokenNode prevIdentifierNode, TokenNode tokenNode) {
    if (tokenNode->next == NULL)
        return;

    char *prevIdentifierValue = prevIdentifierNode->token->value;
    TokenNode currNode = tokenNode->next;
    char *tokenValue = currNode->token->value;
    if (currNode->token->type == TokenPunctuator) {
        if (strcmp(tokenValue, "[") == 0) { // array
            if (prevIdentifierNode->token->type == TokenPunctuator &&
                (strcmp(prevIdentifierValue, "*") == 0 || strcmp(prevIdentifierValue, "&") == 0)) {
                    return;
                }
            tokenNode = processArray(tokenList, arrayList, prevIdentifierNode, tokenNode);
        } else
        if (strcmp(tokenValue, "(") == 0) { // function
            tokenNode = processFunction(tokenList, tokenNode, arrayList);
        } else
        if (strcmp(tokenValue, "=") == 0) { // equals
            processVarAssign(tokenList, arrayList, prevIdentifierNode, tokenNode);
        }
    }
    if (currNode->token->type == TokenIdentifier && currNode->next != NULL) {
        char *nextIdentifierName = currNode->token->value;
        currNode = currNode->next;
        TokenNode nextNode;
        if (currNode->token->type == TokenPunctuator && strcmp(currNode->token->value, "[") == 0) { // array declaration
            setTokenValue(tokenNode->token, nextIdentifierName);
            addGlobalArray(tokenList, tokenNode, arrayList, FALSE);
            currNode = tokenNode->next;
            tokenNode->next = currNode->next;
            deleteTokenNode(currNode);
        }
    }
    return;
}

void processTokenPunctuator(TokenList tokenList, char **arrayList, TokenNode prevNode, TokenNode tokenNode) {
    if (tokenNode->next == NULL)
        return;
    
    char *tokenValue = tokenNode->token->value;
    char *tokenWhitespace = tokenNode->token->preWhitespace;
    if (strcmp(tokenValue, "[") == 0 && tokenWhitespace != NULL && strlen(tokenWhitespace) > 0) { // array short form
        addTokenAfterNode(prevNode, newIdentifierToken("array"), NULL, NULL);
        setTokenValue(tokenNode->token, "(");
        processFunction(tokenList, prevNode->next, arrayList);
    }
}



void processVarAssign(TokenList tokenList, char **arrayList, TokenNode prevIdentifierNode, TokenNode varNameNode) {
    TokenNode assignStart = varNameNode->next->next;
    char *assignStartStr = assignStart->token->value;
    if (assignStart->token->type == TokenIdentifier) {
        if (strcmp(assignStartStr, "array") == 0 ||
            strcmp(assignStartStr, "arrayArgs") == 0 ||
            strcmp(assignStartStr, "array_fill") == 0 ||
            strcmp(assignStartStr, "array_flip") == 0 ||
            strcmp(assignStartStr, "array_keys") == 0 ||
            strcmp(assignStartStr, "array_slice") == 0 ||
            strcmp(assignStartStr, "array_values") == 0 ||
            strcmp(assignStartStr, "range") == 0) {
                TokenNode oldPrevNode = prevIdentifierNode;
                prevIdentifierNode = addGlobalArray(tokenList, prevIdentifierNode, arrayList, TRUE);
                if (oldPrevNode == prevIdentifierNode) {
                    addTokenAfterNode(prevIdentifierNode, newIdentifierToken("void"), NULL, NULL);
                    addTokenAfterNode(prevIdentifierNode->next, newPunctuatorToken("*"), " ", NULL);
                }
        }
    } else
    if (assignStart->token->type == TokenPunctuator) {
        if (strcmp(assignStartStr, "[") == 0) {
            prevIdentifierNode = addGlobalArray(tokenList, prevIdentifierNode, arrayList, TRUE);
            addTokenAfterNode(prevIdentifierNode, newIdentifierToken("void"), NULL, NULL);
            addTokenAfterNode(prevIdentifierNode->next, newPunctuatorToken("*"), " ", NULL);
        }
    }
}



TokenNode processFunction(TokenList tokenList, TokenNode funcNameNode, char **arrayList) {
    int nestedBrackets = 0;
    char *funcName = funcNameNode->token->value;
    TokenNode varStartTokens[MAX_VARS_IN_FUNC];
    int numElements = 1;
    int numVars = 1;
    TokenNode endFuncNode = funcNameNode;
    TokenNode prevLastVar = funcNameNode->next;
    varStartTokens[0] = funcNameNode->next->next;
    int funcIsArray = (strcmp(funcName, "array") == 0);
    if (funcIsArray && prevLastVar && prevLastVar->next) {
        TokenNode varStart = prevLastVar->next;
        if (varStart->token->type == TokenPunctuator) {
            char *varStartValue = varStart->token->value;
            if (strcmp(varStartValue, "]") == 0) {
                setTokenValue(varStart->token, ")");
                return funcNameNode;
            }
            if (strcmp(varStartValue, ")") == 0) {
                return funcNameNode;
            }
        }
    }
    while (endFuncNode->next != NULL) {
            endFuncNode = endFuncNode->next;
            if (endFuncNode->token->type == TokenPunctuator) {
                if (funcIsArray && nestedBrackets == 1) {
                    if (strcmp(endFuncNode->token->value, "=>") == 0) {
                        setTokenValue(endFuncNode->token, ",");
                        if (endFuncNode->token->preWhitespace != NULL)
                            endFuncNode->token->preWhitespace[0] = '\0';
                    } else
                    if (strcmp(endFuncNode->token->value, ",") == 0 ||
                        strcmp(endFuncNode->token->value, ")") == 0 ||
                        strcmp(endFuncNode->token->value, "]") == 0) {
                        if (numVars % 2) {
                            prevLastVar = addTokenAfterNode(prevLastVar, newIdentifierToken("NULL"), NULL, NULL);
                            prevLastVar = addTokenAfterNode(prevLastVar, newPunctuatorToken(","), NULL, NULL);
                            prevLastVar = addTokenAfterNode(prevLastVar, newNumberToken("0"), " ", NULL);
                            prevLastVar = addTokenAfterNode(prevLastVar, newPunctuatorToken(","), NULL, NULL);

                            numVars++;

                            if (prevLastVar->next->token->preWhitespace == NULL) {
                                prevLastVar->next->token->preWhitespace = malloc(2);
                                strcpy(prevLastVar->next->token->preWhitespace, " ");
                            }
                        }
                        prevLastVar = endFuncNode;
                    }
                }

                if (strcmp(endFuncNode->token->value, ")") == 0) {
                    nestedBrackets--;
                } else
                if (strcmp(endFuncNode->token->value, "(") == 0) {
                    nestedBrackets++;
                } else
                if (strcmp(endFuncNode->token->value, "]") == 0) {
                    nestedBrackets--;
                    if (nestedBrackets == 0) {
                        setTokenValue(endFuncNode->token, ")");
                        break;
                    }
                } else
                if (strcmp(endFuncNode->token->value, "[") == 0) {
                    nestedBrackets++;
                } else
                if (strcmp(endFuncNode->token->value, ",") == 0 && nestedBrackets == 1) {
                    varStartTokens[numElements] = endFuncNode->next;
                    numElements++;
                    numVars++;
                }

                if (nestedBrackets == 0)
                    break;
            }
        }

    if (strcmp(funcName, "array") == 0) {
        TokenNode currNode;
        if (numElements > 0)
            setTokenValue(funcNameNode->token, "arrayArgs");
        for (int i = 0; i < numElements; i++) {
            currNode = varStartTokens[i];
            processVarType(currNode, arrayList);
        }
    } else
    if (strcmp(funcName, "sizeof") == 0 ||
        strcmp(funcName, "size_of") == 0 ||
        strcmp(funcName, "sizeOf") == 0) {
        setTokenValue(funcNameNode->token, "count");
        // 1 - Countable|Array
        processVarType(varStartTokens[0], arrayList);
    } else
    if (strcmp(funcName, "array_key_exists") == 0) {
        // 1 - Key
        processVarType(varStartTokens[0], arrayList);
    } else
    if (strcmp(funcName, "array_search") == 0 ||
        strcmp(funcName, "in_array") == 0) {
        // 1- Mixed
        processVarType(varStartTokens[0], arrayList);
    } else
    if (strcmp(funcName, "count") == 0) {
        // 1 - Countable|Array
        processVarType(varStartTokens[0], arrayList);
    } else
    if (strcmp(funcName, "range") == 0) {
        // 1, 2 - Key
        // 3 - Num
        processVarType(varStartTokens[1], arrayList);
    } else
    if (strcmp(funcName, "array_keys") == 0 ||
        strcmp(funcName, "array_unshift") == 0) {
        // Optional 2+... - Mixed
        for (int i = 1; i < numElements; i++)
            processVarType(varStartTokens[i], arrayList);
    } else
    if (strcmp(funcName, "array_push") == 0) {
        // 2++... - Mixed
        for (int i = 1; i < numElements; i++)
            processVarType(varStartTokens[i], arrayList);
    } else
    if (strcmp(funcName, "array_fill") == 0) {
        // 3 - Mixed
        processVarType(varStartTokens[2], arrayList);
    } else
    {
        return endFuncNode;
    }

    return endFuncNode;
}

TokenNode processVarType(TokenNode varStart, char **arrayList) {
    int varType = getDataType(varStart, arrayList);
    char varTypeStr[MAX_DATA_TYPE_LEN + 1];
    sprintf(varTypeStr, "%d", varType);

    TokenNode varEnd = getEndOfVar(varStart, varType);
    varEnd = addTokenAfterNode(varEnd, newPunctuatorToken(","), NULL, NULL);
    varEnd = addTokenAfterNode(varEnd, newNumberToken(varTypeStr), " ", NULL);
    
    return varEnd;
}

TokenNode getEndOfVar(TokenNode varStart, int varType) {
    TokenNode currNode = varStart->next;
    TokenNode prevNode = varStart;

    if (varType == DataTypeLiteral) {
        for (; currNode->next != NULL; currNode = currNode->next) {
            if (currNode->token->type == TokenPunctuator &&
                strcmp(currNode->token->value, "\"") == 0) {
                return currNode;
            }
        }
    }

    int puncCount = 0;
    char *tokenVal = varStart->token->value;
    if (varStart->token->type == TokenPunctuator) {
        if (strcmp(tokenVal, "(") == 0 ||
            strcmp(tokenVal, "[") == 0 ||
            strcmp(tokenVal, "{") == 0) {
                puncCount++;
        }
    }
    for (; currNode->next != NULL; prevNode = currNode, currNode = currNode->next) {
        if (currNode->token->type != TokenPunctuator) {
            continue;
        }

        tokenVal = currNode->token->value;
        if (strcmp(tokenVal, "(") == 0 ||
            strcmp(tokenVal, "[") == 0 ||
            strcmp(tokenVal, "{") == 0) {
                puncCount++;
        } else
        if (strcmp(tokenVal, ")") == 0 ||
            strcmp(tokenVal, "]") == 0 ||
            strcmp(tokenVal, "}") == 0) {
                puncCount--;
        } else {
            if (puncCount != 0)
                continue;
            if (strcmp(tokenVal, ",") == 0 ||
                strcmp(tokenVal, ";") == 0) {
                break;
            }
        }

        if (puncCount < 0 && currNode->token->type == TokenPunctuator)
            break;
    }

    return prevNode;
}



TokenNode processArray(TokenList tokenList, char **arrayList, TokenNode prevNameNode, TokenNode nameNode) {    
    // start processing array
    // name[key1][key2]           => arrayGet(name, key1, keyType1, key2, keyType2)
    // name[key1][key2] = element => arraySet(name, key1, keyType1, key2, keyType2, element, elementType)

    addGlobalArray(tokenList, prevNameNode, arrayList, FALSE);

    TokenNode currNode = prevNameNode;
    TokenNode prevNode;
    TokenNode nextNode;
    TokenNode prevKeyStart;
    char valueTypeStr[MAX_DATA_TYPE_LEN];

    currNode = addTokenAfterNode(currNode, newIdentifierToken(ARRAY_GET), NULL, NULL);
    currNode = addTokenAfterNode(currNode, newPunctuatorToken("("), NULL, NULL);
    currNode->next = nameNode;
    currNode = nameNode->next;

    // start processing keys
    for (prevKeyStart; (prevKeyStart = processArrayKey) != NULL; currNode = prevKeyStart->next) {
        prevKeyStart = processArrayKey(tokenList, arrayList, currNode);
        if (prevKeyStart->next->token->type != TokenPunctuator ||
        strcmp(prevKeyStart->next->token->value, "[") != 0) {
            break;
        }
    }
    currNode = prevKeyStart;

    nextNode = currNode->next;
    if (nextNode->token->type == TokenPunctuator) {
        if (strcmp(nextNode->token->value, "=") == 0) {
            currNode = addTokenAfterNode(currNode, newPunctuatorToken(","), NULL, NULL);
            strcpy(prevNameNode->next->token->value, ARRAY_SET);
            currNode->next = nextNode->next;
            deleteTokenNode(nextNode);
            currNode = currNode->next;
            int varType = getDataType(currNode, arrayList);
            sprintf(valueTypeStr, "%d", varType);

            currNode = getEndOfVar(currNode, varType);
            currNode = addTokenAfterNode(currNode, newPunctuatorToken(","), NULL, NULL);
            currNode = addTokenAfterNode(currNode, newNumberToken(valueTypeStr), " ", NULL);
        }
    }
    currNode = addTokenAfterNode(currNode, newPunctuatorToken(")"), NULL, NULL);
    return currNode;
}

TokenNode processArrayKey(TokenList tokenList, char **arrayList, TokenNode keyStartNode) {
    if (keyStartNode == NULL)
        return NULL;
    TokenNode currNode = keyStartNode;
    TokenNode prevNode = currNode;
    char valueTypeStr[MAX_DATA_TYPE_LEN];

    setTokenValue(currNode->token, ",");
    currNode = currNode->next;
    sprintf(valueTypeStr, "%d", getDataType(currNode, arrayList));

    for (; currNode != NULL; prevNode = currNode, currNode = currNode->next) {
        if (currNode->token->type == TokenIdentifier) {
            processTokenIdentifier(tokenList, arrayList, prevNode, currNode);
        } else
        if (currNode->token->type == TokenPunctuator &&
            strcmp(currNode->token->value, "]") == 0) {
            if (prevNode->token->type == TokenPunctuator && strcmp(prevNode->token->value, ",") == 0)
                addTokenAfterNode(prevNode, newIdentifierToken("NULL"), " ", NULL);
            setTokenValue(currNode->token, ",");
            return addTokenAfterNode(currNode, newNumberToken(valueTypeStr), NULL, NULL);
        }
    }

    return currNode;
}

TokenNode addGlobalArray(TokenList tokenList, TokenNode prevArrayName, char **arrayList, int isNull) {
    if (tokenList == NULL || tokenList->head == NULL || prevArrayName == NULL || prevArrayName->token == NULL)
        return NULL;
    
    char *varName = prevArrayName->next->token->value;

    int i = 0;
    char whitespace[2] = " ";
    for (; arrayList[i] != NULL; i++) {
        if (strcmp(arrayList[i], varName) == 0) {
            if (isNull == FALSE) {
                return prevArrayName;
            }

            TokenNode currNode = prevArrayName;
            TokenNode arrayName = prevArrayName->next;

            char *arrayWhitespace = lastLineOfWhitespace(arrayName->token->preWhitespace);

            currNode = addTokenAfterNode(currNode, newIdentifierToken("arrayDelete"), NULL, NULL);
            currNode = addTokenAfterNode(currNode, newPunctuatorToken("("), NULL, NULL);
            currNode = addTokenAfterNode(currNode, newIdentifierToken(varName), NULL, NULL);
            currNode = addTokenAfterNode(currNode, newPunctuatorToken(")"), NULL, NULL);
            currNode = addTokenAfterNode(currNode, newPunctuatorToken(";"), NULL, NULL);

            arrayName->token->preWhitespace = arrayWhitespace;
            
            return currNode;
        }
    }
    whitespace[0] = '\n';

    arrayList[i] = malloc(strlen(varName) + 1);
    strcpy(arrayList[i], varName);
    arrayList[i+1] = NULL;

    TokenNode oldHead = tokenList->head;

    TokenNode currNode = newTokenNode(newIdentifierToken("void"), NULL, NULL);
    tokenList->head = currNode;
    currNode->next = oldHead;
    

    currNode = addTokenAfterNode(currNode, newPunctuatorToken("*"), NULL, NULL);
    currNode = addTokenAfterNode(currNode, newIdentifierToken(varName), " ", NULL);
    if (! isNull) {
        currNode = addTokenAfterNode(currNode, newPunctuatorToken("="), " ", NULL);
        currNode = addTokenAfterNode(currNode, newIdentifierToken("array"), " ", NULL);
        currNode = addTokenAfterNode(currNode, newPunctuatorToken("("), NULL, NULL);
        currNode = addTokenAfterNode(currNode, newPunctuatorToken(")"), NULL, NULL);
    }
    currNode = addTokenAfterNode(currNode, newPunctuatorToken(";"), NULL, NULL);
    
    oldHead->token->preWhitespace = malloc(strlen(whitespace) + 1);
    strcpy(oldHead->token->preWhitespace, whitespace);

    return prevArrayName;
}



int getDataType(TokenNode tokenNode, char **arrayList) {
    Mixed value = tokenNode->token->value;
    switch (tokenNode->token->type) {
        case TokenNumber:
            return DataTypeInt;
        case TokenIdentifier:
            if (strcmp(value, "array") == 0) {
                return DataTypeArray;
            }
            if (strcmp(value, "NULL") == 0) {
                return DataTypePointer;
            }
            if (arrayList != NULL) {
                for (int i = 0; arrayList[i] != NULL; i++) {
                    if (strcmp(value, arrayList[i]) == 0) {
                        if (tokenNode->next != NULL &&
                        tokenNode->next->token->type == TokenPunctuator &&
                        strcmp(tokenNode->next->token->value, "[") == 0) {
                            return DataTypeUnknown;
                        }
                        return DataTypeArray;
                    }
                }
            }
            return DataTypeUnknown;
        case TokenPunctuator:
            if (strcmp(value, "(") == 0) {
                return getDataType(tokenNode->next, arrayList);
            }
            if (strcmp(value, "\"") == 0) {
                return DataTypeLiteral;
            }
            if (strcmp(value, "'") == 0) {
                return DataTypeChar;
            }
            if (strcmp(value, "[") == 0) {
                return DataTypeArray;
            }
            if (strcmp(value, "-") == 0) {
                if (tokenNode->next->token->type == TokenNumber)
                    return DataTypeInt;
            }
            return DataTypeUnknown;
        default:
            return DataTypeUnknown;
    }
}

char *lastLineOfWhitespace(char *oldWhitespace) {
    char *newWhitespace = NULL;

    if (oldWhitespace != NULL) {
        int oldWhitespaceLen = strlen(oldWhitespace);
        int i = oldWhitespaceLen;

        for (i--; i > -1; i--) {
            if (oldWhitespace[i] == '\n')
                break;
        }
        i++;
        newWhitespace = malloc(strlen(oldWhitespace + i) + 2);
        
        strcpy(newWhitespace, "\n");
        strcat(newWhitespace, oldWhitespace + i);
    } else {
        newWhitespace = malloc(2);
        strcpy(newWhitespace, "\n");
    }

    return newWhitespace;
}
