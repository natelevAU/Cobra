#include "preprocess.h"

void preProcess(char *sourceStr, int *BytesRead, TokenList tokenList) {
    int sourceLen = *BytesRead;

    char *currString = malloc(MAX_STRING_LEN); // TODO Realloc
    currString = memset(currString, '\0', MAX_STRING_LEN);
    int currStringPos = 0;
    
    char *whitespace = malloc(MAX_SPACES); // TODO Realloc
    whitespace = memset(whitespace, '\0', MAX_SPACES);
    int whitespacePos = 0;

    // do preprocessing + tokenising
    int currChar = 0;
    int lastChar;
    int nextChar;
    int tokenLen = 0;
    for (int sourcePos = 0; (sourcePos + 1) < sourceLen; sourcePos++) {
        lastChar = currChar;
        currChar = sourceStr[sourcePos];
        nextChar = sourceStr[sourcePos+1];
        
        // Replace NUL (0) with whitespace
        if (currChar == 0) {
            addCharToStr(whitespace, &whitespacePos, ' ');
        } else
        // Continue lines with '\'
        if (currChar == '\\' && nextChar == '\n') {
            sourcePos++; // skip newline
            addCharToStr(whitespace, &whitespacePos, ' ');
        } else
        // Comments
        if (tokenLen = isComment(sourceStr, sourcePos, sourceLen)) {
            sourcePos += tokenLen;
            addCharToStr(whitespace, &whitespacePos, '\n');
        } else
        // Single quotes
        if (currChar == '\'') {
            addToken(tokenList, newPunctuatorToken("'"), whitespace, &whitespacePos);
            for (sourcePos++; (sourcePos + 1) < sourceLen; sourcePos++) {
                if (sourceStr[sourcePos] == '\'' && sourceStr[sourcePos-1] != '\\') break;
                addCharToStr(currString, &currStringPos, sourceStr[sourcePos]);
            }
            addToken(tokenList, newLiteralToken(currString), whitespace, &whitespacePos);
            addToken(tokenList, newPunctuatorToken("'"), whitespace, &whitespacePos);
            memset(currString, '\0', currStringPos);
            currStringPos = 0;
        } else
        // Double quotes
        if (currChar == '"') {
            addToken(tokenList, newPunctuatorToken("\""), whitespace, &whitespacePos);
            for (sourcePos++; (sourcePos + 1) < sourceLen; sourcePos++) {
                if (sourceStr[sourcePos] == '"' && sourceStr[sourcePos-1] != '\\') break;
                addCharToStr(currString, &currStringPos, sourceStr[sourcePos]);
            }
            addToken(tokenList, newLiteralToken(currString), whitespace, &whitespacePos);
            addToken(tokenList, newPunctuatorToken("\""), whitespace, &whitespacePos);
            memset(currString, '\0', currStringPos);
            currStringPos = 0;
        } else
        // Punctuation
        if (tokenLen = isPunctuator(sourceStr, sourcePos, sourceLen)) {
            for (currStringPos = 0; currStringPos < tokenLen; sourcePos++) {
                addCharToStr(currString, &currStringPos, sourceStr[sourcePos]);
            }
            sourcePos--;
            addToken(tokenList, newPunctuatorToken(currString), whitespace, &whitespacePos);
            memset(currString, '\0', currStringPos);
            currStringPos = 0;
        } else
        // Numbers
        if (tokenLen = isNumber(sourceStr, sourcePos, sourceLen)) {
            for (currStringPos = 0; currStringPos < tokenLen; sourcePos++) {
                addCharToStr(currString, &currStringPos, sourceStr[sourcePos]);
            }
            sourcePos--;
            addToken(tokenList, newNumberToken(currString), whitespace, &whitespacePos);
            memset(currString, '\0', currStringPos);
            currStringPos = 0;
        } else
        // Identifiers
        if (tokenLen = isIdentifier(sourceStr, sourcePos, sourceLen)) {
            for (currStringPos = 0; currStringPos < tokenLen; sourcePos++) {
                addCharToStr(currString, &currStringPos, sourceStr[sourcePos]);
            }
            sourcePos--;
            addToken(tokenList, newIdentifierToken(currString), whitespace, &whitespacePos);
            memset(currString, '\0', currStringPos);
            currStringPos = 0;
        } else
        // Whitespace
        if (currChar == ' ' || currChar == '\t' || currChar == '\n' || currChar == '\v' || currChar == '\f' || currChar == '\r') {
            addCharToStr(whitespace, &whitespacePos, currChar);
        } else {
            addToken(tokenList, newOtherToken(sourceStr[sourcePos]), whitespace, &whitespacePos);
        }
    }
}



void addCharToStr(char *str, int *pos, char c) {
    str[*pos] = c;
    (*pos)++;
    str[*pos] = '\0';
}

void addWordToStr(char *str, int *pos, const char *word) {
    for (int i = 0; i < strlen(word); i++) {
        str[*pos] = word[i];
        (*pos)++;
    }
}

int getEndOfLine(int pos, char *str, int len) {
    for (; str[pos] != '\n' && pos < len; pos++) {
        if (str[pos] == '\\' && (pos + 1) < len && str[pos + 1] == '\n') {
            pos++;
        } else
        if (str[pos] == '\n') {
            return pos;
        }
        
    }
    return pos;
}



int isComment(char *sourceStr, int sourcePos, int sourceLen) {
    // Single line comments
    char currChar = sourceStr[sourcePos];
    char nextChar = (sourcePos + 1 < sourceLen) ? sourceStr[sourcePos + 1] : '\0';
    if (currChar == '/' && nextChar == '/') {
        return getEndOfLine(sourcePos, sourceStr, sourceLen) - sourcePos;
    } else
    // Multi line comments
    if (currChar == '/' && nextChar == '*') {
        int i = 0;
        for (; (sourcePos + 1) < sourceLen; sourcePos++, i++) {
            if (sourceStr[sourcePos] == '*' && sourceStr[sourcePos+1] == '/') {
                sourcePos++;
                i++;
                break;
            }
        }
        return i;
    }
    return 0;
}

int isIdentifier(char *sourceStr, int sourcePos, int sourceLen) {
    int identifierLen = 0;
    char c = sourceStr[sourcePos];
    if (isalpha(c) || c == '_' || c == '$') {
        identifierLen++;
        for (int i = sourcePos + 1; i < sourceLen; i++, identifierLen++) {
            c = sourceStr[i];
            if (isdigit(c)) continue;
            if (isalpha(c)) continue;
            if (c == '_' || c == '$') continue;

            break;
        }
    }
    return identifierLen;
}

int isNumber(char *sourceStr, int sourcePos, int sourceLen) {
    int numLen = 0;
    char c = sourceStr[sourcePos];
    char nextC = (sourcePos + 1 < sourceLen) ? sourceStr[sourcePos + 1] : '\0';
    char prevC;
    if (c == '-' && isdigit(nextC)) {
        numLen = 2;
    } else
    if (isdigit(c)) {
        numLen = 1;
    } else {
        return 0;
    }

    for (int i = sourcePos + numLen; i < sourceLen; numLen++, i++) {
        prevC = c;
        c = (i < sourceLen) ? sourceStr[i] : '\0';

        if (isdigit(c)) continue;
        if (c == '_' || c == '.') continue;
        if (c == '-' || c == '+') {
            if (prevC == 'e' || prevC == 'E' || prevC == 'p' || prevC == 'P') {
                continue;
            } else {
                break;
            }
        }
        if (isalpha(c)) continue;

        break;
    }
    return numLen;
}

int isPunctuator(char *sourceStr, int sourcePos, int sourceLen) {
    int punctuatorLen = 0;
    switch(sourceStr[sourcePos]) {
        case '<':
        case '>':
            if (sourcePos + 1 < sourceLen && sourceStr[sourcePos + 1] == sourceStr[sourcePos]) {
                if (sourcePos + 2 < sourceLen && sourceStr[sourcePos + 2] == '=') {
                    punctuatorLen = 3;
                } else {
                    punctuatorLen = 2;
                }
                break;
            }
            punctuatorLen = 1;
            break;
        case '&':
        case '|':
        case '+':
        case '-':
            if (sourcePos + 1 < sourceLen && sourceStr[sourcePos + 1] == sourceStr[sourcePos]) {
                punctuatorLen = 2;
                break;
            }
        case '*':
        case '/':
        case '%':
        case '^':
        case '=':
            if (sourcePos + 1 < sourceLen && sourceStr[sourcePos] == '=' && sourceStr[sourcePos + 1] == '>') {
                punctuatorLen = 2;
                break;
            }
        case '!':
            if (sourcePos + 1 < sourceLen && sourceStr[sourcePos + 1] == '=') {
                punctuatorLen = 2;
            } else {
                punctuatorLen = 1;
            }
            break;
        case '"':
        case '#':
        case '\'':
        case '(':
        case ')':
        case ',':
        case '.':
        case ':':
        case ';':
        case '?':
        case '[':
        case '\\':
        case ']':
        case '_':
        case '{':
        case '}':
        case '~':
            punctuatorLen = 1;
            break;
    }
    return punctuatorLen;
}
