#include "prelex.h"
#include "preprocess.h"
#include "process.h"
#include "error.h"
#include "token.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>



char* readFile(char* fileName) {
    struct stat fileInfo;
    char *sourceStr;
    FILE *inFile;
    int BytesRead;
    char *p;
    
    if (stat(fileName, &fileInfo))
        programFailNoParser("can't read file %s\n", fileName);
    
    sourceStr = malloc(fileInfo.st_size + 1);
    if (sourceStr == NULL)
        programFailNoParser("out of memory\n");
        
    inFile = fopen(fileName, "r");
    if (inFile == NULL)
        programFailNoParser("can't read file %s\n", fileName);
    
    BytesRead = fread(sourceStr, 1, fileInfo.st_size, inFile);
    if (BytesRead == 0)
        programFailNoParser("can't read file %s\n", fileName);

    sourceStr[BytesRead] = '\0';
    fclose(inFile);
    
    if ((sourceStr[0] == '#') && (sourceStr[1] == '!'))
    {
        for (p = sourceStr; (*p != '\r') && (*p != '\n'); ++p)
        {
            *p = ' ';
        }
    }

    char *processedStr = sourceStr;

    TokenList tokenList = newTokenList();
    preProcess(sourceStr, &BytesRead, tokenList);
    free(sourceStr);

    processTokenList(tokenList);
    
    processedStr = tokenListToString(tokenList);

    deleteTokenList(tokenList);
    
    return processedStr;
}
