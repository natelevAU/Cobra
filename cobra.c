#include "cobra.h"
#include "prelex.h"
#include "picoc.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define PROG_S "cobra"

#define PICOC_STACK_SIZE (128*1024)              /* space for the the stack */

int main(int argc, char **argv)
{
    int ParamCount = 1;
    int DontRunMain = FALSE;
    int StackSize = getenv("STACKSIZE") ? atoi(getenv("STACKSIZE")) : PICOC_STACK_SIZE;
    Picoc pc;
    
    if (argc < 2)
    {
        printf("Format: %s <csource1.c>... [- <arg1>...]    : run a program (calls main() to start it)\n", PROG_S);
        printf("        %s -s <csource1.c>... [- <arg1>...] : script mode - runs the program without calling main()\n", PROG_S);
        printf("        %s -i                               : interactive mode\n", PROG_S);
        exit(1);
    }
    
    PicocInitialise(&pc, StackSize);
    
    if (strcmp(argv[ParamCount], "-s") == 0 || strcmp(argv[ParamCount], "-m") == 0)
    {
        DontRunMain = TRUE;
        PicocIncludeAllSystemHeaders(&pc);
        ParamCount++;
    }
        
    if (argc > ParamCount && strcmp(argv[ParamCount], "-i") == 0)
    {
        PicocIncludeAllSystemHeaders(&pc);
        PicocParseInteractive(&pc);
    }
    else
    {
        IncludeFile(&pc, "stdarray.h");
        if (PicocPlatformSetExitPoint(&pc))
        {
            PicocCleanup(&pc);
            return pc.PicocExitValue;
        }
        
        for (; ParamCount < argc && strcmp(argv[ParamCount], "-") != 0; ParamCount++) {
            char* fileName = argv[ParamCount];
            char* sourceStr = readFile(fileName);
            // printf("%s\n", sourceStr);
            // Preprocessing
            PicocParse(&pc, fileName, sourceStr, strlen(sourceStr), TRUE, FALSE, TRUE, TRUE);
        }
        
        if (!DontRunMain)
            PicocCallMain(&pc, argc - ParamCount, &argv[ParamCount]);
    }
    
    PicocCleanup(&pc);
    return pc.PicocExitValue;
}
