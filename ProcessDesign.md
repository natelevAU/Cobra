# PREPROCESSING

1. Preprocessing Lex
- Convert to Unicode (not sure if necessary)
- Read file and break into lines
- If last line doesn't have newline, emit warning (but implicitly add \n before EOF)
- Merge continued lines (lines ending with '\')
- Replace // comments with single space
- Replace /* */ comments with single space
- Replace NUL with white space

2. Preprocessing Tokens
- Create largest possible tokens
- Whitespace seperates tokens - is not a token itself
- Types of preprocessing tokens are:
    1. Identifiers
        - Any sequence of letters/underscores/digits/$ that begins with a letter/underscore/$
    2. Preprocessing Numbers
        - Integer/floating point constants
        - Begin with an optional period, then a required decimal digit
        - Continue with any sequence of letters/digits/underscores/periods/exponents
        - Exponents are e+ e- E+ E- p+ p- P+ P-
    3. String Literals
        - String constants, character constants, header file names (argument of #include)
        - Contained within "...", '...', or #include <...>
        - Can't extend past the end of a line
    4. Punctuators
        - Any of the following symbols:
        - ! " # % & ' ( ) * + , - . / : ; < = > ? [ \ ] ^ _ { | } ~
        - ++ -- += -= *= /= %= &= ^= |= == != <= >= && || << >>
        - <<= >>=
        ADDED:
        - =>
    5. Other
        - Any single byte that doesn't fit into the above categories
        - Includes ASCII characters @ $ ` and control characters other than NUL
        - Includes all high bit set characters 0x7F to 0xFF
        - Most likely trigger an error

3. Process Preprocessing Language
    - Preprocessing directives (lines starting with #)
        - #define #include #undef #ifdef #ifndef #if #else #elif #endif #error #pragma
    - Macro expansion
    - Issue any relevant errors/warnings
