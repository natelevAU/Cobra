#include "error.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>



void programFailNoParser(const char* errorStringFormat, ...) {
    va_list Args;
    va_start(Args, errorStringFormat);
    PlatformVPrintf(stderr, errorStringFormat, Args);
    va_end(Args);
    // TODO Implement this
    exit(1);
    printf("\n");
}
