#pragma once
#include <stdlib.h>
#include <string.h>

typedef struct binTreeNode *BinTreeNode;
typedef struct binTree *BinTree;

struct binTreeNode {
    struct binTreeNode* left;
    struct binTreeNode* right;
    BinTree child;
    char* name;
    void* value;
    int id;
};

struct binTree {
    BinTreeNode root;
    void* id;
    int numNodes;
};

#include "interpreter.h"

BinTreeNode newBinTreeNode(const char* name, void* value);
BinTree newBinTree();
BinTree newBinTreeID(void* id);
void binTreeNodeSetValue(BinTreeNode node, void* value);
void binTreeNodeSetId(BinTreeNode node, int value);
void deleteBinTreeNode(BinTreeNode node);
void deleteBinTree(BinTree tree);

BinTreeNode binTreeSearch(BinTree tree, const char* name);
BinTreeNode binTreeGet(BinTree tree, const char* name);
BinTreeNode binTreeAddNode(BinTree tree, const char* name, void* value);
BinTree binNodeAddTree(BinTreeNode node);
