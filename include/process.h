#ifndef PROCESS_h
#define PROCESS_H

#include "token.h"


void processTokenList(TokenList tokenList);
void processToken(TokenList tokenList, char **arrayList, TokenNode prevNode, TokenNode currNode);

void processTokenIdentifier(TokenList tokenList, char **arrayList, TokenNode prevNode, TokenNode tokenNode);
void processTokenPunctuator(TokenList tokenList, char **arrayList, TokenNode prevNode, TokenNode tokenNode);

void processVarAssign(TokenList tokenList, char **arrayList, TokenNode prevIdentifierNode, TokenNode varNameNode);

TokenNode processFunction(TokenList tokenList, TokenNode funcNameNode, char **arrayList);
TokenNode processVarType(TokenNode varStart, char **arrayList);
TokenNode getEndOfVar(TokenNode varStart, int varType);

TokenNode processArray(TokenList tokenList, char **arrayList, TokenNode prevNode, TokenNode nameNode);
TokenNode processArrayKey(TokenList tokenList, char **arrayList, TokenNode keyStartNode);
TokenNode addGlobalArray(TokenList tokenList, TokenNode prevArrayName, char **arrayList, int isNull);

int getDataType(TokenNode tokenNode, char **arrayList);
char *lastLineOfWhitespace(char *oldWhitespace);


#endif