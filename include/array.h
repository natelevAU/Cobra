#pragma once

#include <stdint.h>

#define IS_NUMERIC_DATA_TYPE(t) (((t) == DataTypeInt) || ((t) == DataTypeDouble))
#define IS_INTEGER_NUMERIC_DATA_TYPE(t) (IS_NUMERIC_DATA_TYPE(t->BaseDataType))


#define COUNT_NORMAL 0
#define COUNT_RECURSIVE 1

#define MAX_DATA_TYPE_LEN 2

enum DataType
{
    DataTypeUnknown,
    DataTypePointer,
    DataTypeArray,
    DataTypeLiteral,
    DataTypeString,
    DataTypeChar,
    DataTypeInt,
    DataTypeFloat,
    DataTypeDouble
};

#define TRUE 1
#define FALSE 0


typedef enum DataType DataType;
typedef struct array *Array;
typedef struct arrayNode *ArrayNode;
typedef long Num;
typedef void *Key;
typedef void *Mixed;
typedef void *Object;

struct array {
    DataType type;
    int size;
    int highestKey;
    ArrayNode head;
    ArrayNode tail;
    ArrayNode current;
};

struct arrayNode {
    DataType type;
    Key key;
    Mixed element;
    int keyType;
    int elementType;
    ArrayNode next;
    ArrayNode prev;
};



Array newArray();
ArrayNode newArrayNode(Key key, int keyType, Mixed element, int elementType);
void deleteArrayNode(ArrayNode arrayNode);
void deleteArray(Array array);

Mixed arrayCurrent(Array array);
int arrayCurrentType(Array array);
Mixed setArrayPointer(Array array, ArrayNode arrayNode);
Mixed setArrayPointerFirst(Array array);
Mixed setArrayPointerLast(Array array);
Mixed setArrayPointerNext(Array array);
Mixed setArrayPointerPrev(Array array);
void setArrayNodeKey(ArrayNode arrayNode, Key key, int keyType);
void setArrayNodeElement(ArrayNode arrayNode, Mixed element, int elementType);

void arrayPrint(Array array);
void varPrint(Mixed data, int dataType);
void swapArrayNodes(Array array, ArrayNode node1, ArrayNode node2);
int arrayCount(Array array, int mode);

ArrayNode addArrayNode(Array array, ArrayNode arrayNode);
ArrayNode addArrayNodeStart(Array array, ArrayNode arrayNode);
ArrayNode addArrayNodeEnd(Array array, ArrayNode arrayNode);
ArrayNode addArrayNodeAfter(Array array, ArrayNode newNode, ArrayNode arrayNode);
ArrayNode removeArrayNode(Array array, ArrayNode arrayNode);
ArrayNode removeArrayNodeStart(Array array);
ArrayNode removeArrayNodeEnd(Array array);

ArrayNode getArrayNodeByKey(Array array, Key key, DataType keyType, int initialise);
ArrayNode getArrayNodeByElement(Array array, Mixed element, DataType elementType, int initialise);
ArrayNode getArrayNodeByIndex(Array array, int index);
ArrayNode copyArrayNode(ArrayNode arrayNode, int preserve_keys);

int keyCmp(ArrayNode arrayNode, Key key, DataType keyType);
int keyComparable(ArrayNode arrayNode, Key key, DataType keyType);
int elementCmp(ArrayNode arrayNode, Mixed element, DataType elementType);
int elementComparable(ArrayNode arrayNode, Mixed element, DataType elementType);
int arraySort(Array array, int by_val, int direction_up);

int isStrNum(char *str);
int isNum(void *data, int dataType);
int isStr(void *data, int dataType);
int isKey(void *data, int dataType);

int calcBitSize(Object value);
int calcIsNum(Object value, int valueType);
int calcIsArray(Object value, int valueType);
int calcNumType(Object value, int valueType);
int calcPointerType(void *value, int valueType);
int calcKeyType(Object value, int valueType);
int calcMixedType(Object value, int valueType);

