#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define EOF -1

#define TRUE 1
#define FALSE 0

#define MAX_STRING_LEN 1024
#define MAX_SPACES 1024


typedef enum tokenType {
    TokenOther = 0,
    TokenIdentifier = 1,
    TokenNumber = 2,
    TokenLiteral = 3,
    TokenPunctuator = 4
} TokenType;

typedef struct token {
    char *value;
    char *preWhitespace;
    TokenType type;
    int len;
} *Token;

typedef struct tokenNode {
    Token token;
    struct tokenNode *next;
} *TokenNode;

typedef struct tokenList {
    TokenNode head;
    TokenNode tail;
    int len;
} *TokenList;



TokenList newTokenList();
TokenNode newTokenNode(Token token, char *whitespace, int *whitespacePos);
void deleteTokenNode(TokenNode tokenNode);
void deleteTokenList(TokenList tokenList);

Token newToken();
Token newIdentifierToken(char *identifier);
Token newNumberToken(char *number);
Token newLiteralToken(char *literal);
Token newPunctuatorToken(char *punctuator);
Token newOtherToken(char c);
void deleteToken(Token token);

void addToken(TokenList tokenList, Token token, char *whitespace, int *whitespacePos);
TokenNode addTokenAfterNode(TokenNode tokenNode, Token token, char *whitespace, int *whitespacePos);
void swapTokenNodes(TokenNode prevToken1, TokenNode prevToken2);

void calcTokenListLen(TokenList tokenList);
int calcTokenLen(Token token);
char *tokenListToString(TokenList tokenList);
void printToken(Token token, char *string);

void setTokenValue(Token token, char *newValue);



