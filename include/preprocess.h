#pragma once
#include "token.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void preProcess(char *sourceStr, int *BytesRead, TokenList tokenList);

void addCharToStr(char *str, int *pos, char c);
void addWordToStr(char *str, int *pos, const char *word);
int getEndOfLine(int pos, char *str, int len);

int isComment(char *sourceStr, int sourcePos, int sourceLen);
int isIdentifier(char *sourceStr, int sourcePos, int sourceLen);
int isNumber(char *sourceStr, int sourcePos, int sourceLen);
int isPunctuator(char *sourceStr, int sourcePos, int sourceLen);
