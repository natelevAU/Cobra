#include <stdio.h>

int main(void) {

    printf("Empty array %d\n", array_sum(array()));
    
    first_array = [0, "three"];
    printf("Sum %d\n", array_sum(first_array));

    array_push(first_array, 20, 50);
    printf("Sum %d\n", array_sum(first_array));

    array_push(first_array, "string", "more string", 70);
    printf("Sum %d\n", array_sum(first_array));

    array_push(first_array, -140, -0);
    printf("Sum %d\n", array_sum(first_array));

    printf("Sum %d\n", array_sum(array_slice(first_array, -1, 3)));

    return 0;
}
