#include <stdio.h>

int main(void) {

    array_print(range(0, 1));
    array_print(range('a', 'j'));

    array_print(range(-5, 10));
    array_print(range('h', 'p'));

    array_print(range(-5, 10, 3));
    array_print(range('h', 'p', 2));

    return 0;
}
