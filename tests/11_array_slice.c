#include <stdio.h>

int main(void) {

    first_array = [0, 1, -1 => 2, "three", "four", "minustwo" => "five"];
    
    second_array = array_slice(first_array, 0);
    array_print(second_array);

    array_print(array_slice(second_array, 2));
    array_print(array_slice(second_array, -2));

    array_print(array_slice(first_array, 2, 3));
    array_print(array_slice(first_array, -2, 3));

    array_print(array_slice(first_array, 2, 4));
    array_print(array_slice(first_array, -2, 4));

    array_print(array_slice(first_array, 2, 5));
    array_print(array_slice(first_array, -2, 5));

    array_print(array_slice(first_array, 5));
    array_print(array_slice(first_array, -6));


    printf("\n");


    array_print(array_slice(second_array, 2, 100, 1));
    array_print(array_slice(second_array, -2, 100, 1));

    array_print(array_slice(first_array, 2, 3, 1));
    array_print(array_slice(first_array, -2, 3, 1));

    array_print(array_slice(first_array, 2, 4, 1));
    array_print(array_slice(first_array, -2, 4, 1));

    array_print(array_slice(first_array, 2, 5, 1));
    array_print(array_slice(first_array, -2, 5, 1));

    array_print(array_slice(first_array, 5, 1, 1));
    array_print(array_slice(first_array, -6, 1, 1));

    return 0;
}
