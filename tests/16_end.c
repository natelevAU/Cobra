#include <stdio.h>

int main(void) {

    first_array = array(0 => 10, 1 => 9);
    printf("%d\n", end(first_array));

    new_array[-1] = 0;

    new_array = array(0 => -5, 1 => "minus six");
    printf("%s\n", end(new_array));

    second_array[0] = 100;
    second_array[1] = 99;
    printf("%d\n", end(second_array));

    third_array = array(20, 30);
    printf("%d\n", end(third_array));

    fourth_array = array(50, "a string key" => "one hundred");
    printf("%s\n", end(fourth_array));

    fifth_array = ["test", 10 => "more tests", 999];
    printf("%d\n", end(fifth_array));

    multi_dimensional_array = ["string", 20 => -50, third_array, fifth_array];
    printf("%d\n", end(end(multi_dimensional_array)));

    return 0;
}
