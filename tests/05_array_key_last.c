#include <stdio.h>

int main(void) {

    empty_array = [];
    if (array_key_last(empty_array) == NULL) {
        printf("NULL\n");
    }

    first_array = [23, -5 => 7];
    printf("%d\n", array_key_last(first_array));

    first_array[-42] = "test";
    printf("%d\n", array_key_last(first_array));

    second_array = ["first_key" => 0, 0 => "middle", "second_key" => 23];
    printf("%s\n", array_key_last(second_array));
    second_array["first_key"] = 1;
    printf("%s\n", array_key_last(second_array));

    third_array = array(-500 => "negative", "zero", "one", -1000 => "large negative");
    printf("%d\n", array_key_last(third_array));

    return 0;
}
