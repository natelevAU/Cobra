#include <stdio.h>

int main(void) {

    first_array = array(0 => 10, 1 => 9);
    printf("%d\n", reset(first_array));

    new_array[-1] = 0;

    new_array = array(0 => "minus six", 1 => -5);
    printf("%s\n", reset(new_array));

    second_array[0] = 100;
    second_array[1] = 99;
    printf("%d\n", reset(second_array));

    third_array = array(20, 30);
    printf("%d\n", reset(third_array));

    fourth_array = array("a string key" => "one hundred", 50);
    printf("%s\n", reset(fourth_array));

    fifth_array = ["test", 10 => "more tests", 999];
    printf("%s\n", reset(fifth_array));

    multi_dimensional_array = ["string", 20 => -50, third_array, fifth_array];
    printf("%s\n", reset(end(multi_dimensional_array)));

    return 0;
}
