#include <stdio.h>

int main(void) {

    first_array = [0, 1, -1 => 2, "three", "four", "minustwo" => "five"];

    array_print(first_array);
    printf("Shift %d\n", array_shift(first_array));
    array_print(first_array);
    printf("Shift %d\n", array_shift(first_array));
    array_print(first_array);
    printf("Shift %d\n", array_shift(first_array));
    array_print(first_array);
    printf("Shift %s\n", array_shift(first_array));
    array_print(first_array);
    printf("Shift %s\n", array_shift(first_array));
    array_print(first_array);
    printf("Shift %s\n", array_shift(first_array));
    array_print(first_array);

    return 0;
}
