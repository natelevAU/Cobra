#include <stdio.h>

int main(void) {

    empty_array = [];
    if (array_key_first(empty_array) == NULL) {
        printf("NULL\n");
    }

    first_array = [23, -5 => 7];
    printf("%d\n", array_key_first(first_array));

    first_array[-42] = "test";
    printf("%d\n", array_key_first(first_array));

    second_array = ["first_key" => 0, 0 => "middle", "second_key" => 23];
    printf("%s\n", array_key_first(second_array));
    second_array["first_key"] = 1;
    printf("%s\n", array_key_first(second_array));

    third_array = array(-500 => "negative", "zero", "one", -1000 => "large negative");
    printf("%d\n", array_key_first(third_array));

    return 0;
}
