#include <stdio.h>

int main(void) {

    first_array = [0];

    array_print(first_array);

    array_unshift(first_array, -1);
    array_print(first_array);

    array_unshift(first_array, 3);
    array_print(first_array);

    array_unshift(first_array, "key");
    array_print(first_array);

    array_unshift(first_array, "another key");
    array_print(first_array);
    
    array_unshift(first_array, 10523, "more keys", 17, "a mix of strings and numbers");
    array_print(first_array);

    return 0;
}
