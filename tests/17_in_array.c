#include <stdio.h>

int main(void) {

    first_array = array(0 => 10, 1 => 9);
    if (in_array(10, first_array)) {
        printf("10 is in first_array\n");
    }
    if (! in_array(0, first_array)) {
        printf("0 is NOT in first_array\n\n");
    }

    new_array[-1] = 0;

    new_array = array(0 => -5, 1 => "minus six");
    if (in_array("minus six", new_array)) {
        printf("minus six is in new_array\n");
    }
    if (! in_array(-1, new_array)) {
        printf("1 is NOT in new_array\n\n");
    }

    second_array[0] = 100;
    second_array[1] = 99;
    if (in_array(100, second_array)) {
        printf("100 is in second_array\n");
    }
    if (! in_array("one hundred", second_array)) {
        printf("one hundred is NOT in second_array\n\n");
    }

    third_array = array(20, 30);
    if (in_array(20, third_array)) {
        printf("20 is in third_array\n");
    }
    if (! in_array(1, third_array)) {
        printf("1 is NOT in third_array\n\n");
    }

    fourth_array = array(50, "a string key" => "one hundred");
    if (in_array("one hundred", fourth_array)) {
        printf("one hundred is in fourth_array\n");
    }
    if (! in_array("onehundred", fourth_array)) {
        printf("onehundred is NOT in fourth_array\n\n");
    }

    fifth_array = ["test", 10 => "more tests", 999];
    if (in_array(999, fifth_array)) {
        printf("999 is in fifth_array\n");
    }
    if (! in_array(1000, fifth_array)) {
        printf("1000 is NOT in fifth_array\n\n");
    }

    multi_dimensional_array = ["string", 20 => -50, third_array, fifth_array];
    if (! in_array(999, multi_dimensional_array)) {
        printf("999 is NOT in multi_dimensional_array\n\n");
    }

    return 0;
}
