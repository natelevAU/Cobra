#include <stdio.h>

int main(void) {

    first_array = [ ];
    array_print(first_array);

    array_push(first_array, "orange");
    array_print(first_array);

    array_push(first_array, "banana", "apple", 100);
    array_print(first_array);

    first_array[4] = "four";
    first_array[-2] = "minustwo";

    array_push(first_array, 5, "six", 7);
    array_print(first_array);

    return 0;
}
