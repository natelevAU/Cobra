#include <stdio.h>

int main(void) {

    first_array = array(0 => 10, 1 => 9);
    printf("%d\n", count(first_array));

    new_array[-1] = 0;

    new_array = array(0 => -5, 1 => -6);
    printf("%d\n", count(new_array));

    second_array[0] = 100;
    second_array[1] = 99;
    printf("%d\n", count(second_array));

    third_array = array(20, 30);
    printf("%d\n", count(third_array));

    fourth_array = array("a string key" => "one hundred", 50);
    printf("%d\n", count(fourth_array));

    fifth_array = ["test", 10 => "more tests", 999];
    printf("%d\n", count(fifth_array));

    empty_array_1 = [];
    printf("%d\n", count(empty_array_1));

    empty_array_2 = array();
    printf("%d\n", count(empty_array_2));

    multi_dimensional_array = ["string", 20 => -50, third_array, fifth_array, empty_array_1];
    printf("%d\n", count(multi_dimensional_array));

    return 0;
}
