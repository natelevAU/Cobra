#include <stdio.h>

int main(void) {

    first_array = array(0 => 10, 1 => 9);
    printf("%d %d\n", first_array[0], first_array[1]);

    new_array[-1] = 0;

    new_array = array(0 => -5, 1 => -6);
    printf("%d %d\n", new_array[0], new_array[1]);

    second_array[0] = 100;
    second_array[1] = 99;
    printf("%d %d\n", second_array[0], second_array[1]);

    third_array = array(20, 30);
    printf("%d %d\n", third_array[0], third_array[1]);

    fourth_array = array("a string key" => "one hundred", 50);
    printf("%d %s\n", fourth_array[0], fourth_array["a string key"]);

    fifth_array = ["test", 10 => "more tests", 999];
    array_print(fifth_array);

    empty_array_1 = [];
    array_print(empty_array_1);

    empty_array_2 = array();
    array_print(empty_array_2);

    multi_dimensional_array = ["string", 20 => -50, third_array, fifth_array, empty_array_1];
    array_print(multi_dimensional_array);

    return 0;
}
