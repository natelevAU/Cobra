#include <stdio.h>

int main(void) {

    empty_array = [];
    empty_array_values = array_values(empty_array);
    array_print(empty_array_values);

    first_array = [23, -5 => 7];
    first_array_values = array_values(first_array);
    array_print(first_array_values);

    first_array[-42] = "test";
    first_array_values = array_values(first_array);
    array_print(first_array_values);

    second_array = ["first_key" => 0, 0 => "middle", "second_key" => 23];
    second_array_values = array_values(second_array);
    array_print(second_array_values);

    second_array["first_key"] = 1;
    second_array_values = array_values(second_array);
    array_print(second_array_values);

    third_array = array(-500 => "negative", "zero", "one", -1000 => "large negative");
    third_array_values = array_values(third_array);
    array_print(third_array_values);

    printf("\n");

    first_array_keys = array_keys(first_array, 7);
    array_print(first_array_keys);

    fourth_array = ["search", 100, "search", "one thousand", "search", -10 => -500, "search"];
    fourth_array_keys = array_keys(fourth_array, "search");
    array_print(fourth_array_keys);

    return 0;
}
