#include <stdio.h>

int main(void) {

    empty_array = [];
    first_array = [0, 1, -1 => 2, "three", "four", "minustwo" => "five"];

    if (! array_search(0, empty_array)) {
        printf("Empty array DOES NOT contain key 0\n");
    }

    if (! array_search("zero", empty_array)) {
        printf("Empty array DOES NOT contain key zero\n");
    }


    printf("%d\n", array_search(0, first_array));
    printf("%d\n", array_search(2, first_array));
    printf("%d\n", array_search("three", first_array));
    printf("%s\n", array_search("five", first_array));

    if (! array_search("zero", first_array)) {
        printf("First array DOES NOT contain key zero\n");
    }
    
    return 0;
}
