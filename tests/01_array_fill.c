#include <stdio.h>

int main(void) {

    first_array = array_fill(0, 1, 20);
    array_print(first_array);

    second_array = array_fill(0, 0, "empty");
    array_print(second_array);

    second_array = array_fill(10, 5, "five");
    array_print(second_array);

    second_array["test"] = "six";
    array_print(second_array);

    second_array = array_fill(-30, 4, 100);
    second_array[1] = "one";
    array_print(second_array);

    third_array = array_fill(-1, 2, second_array);
    array_print(third_array);

    fourth_array = array_fill(100, 1, third_array);
    fourth_array[50] = "multi-dimensional";
    array_print(fourth_array);

    return 0;
}
