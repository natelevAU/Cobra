#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
    a["xaver"] = "ravex";
    a["xavera"] = "ravexa";
    a["xaveron"] = "ravexon";

    printf("%s\n", a["xaver"]);
    printf("%s\n", a["xaveron"]);


    b["test"] = "a new test";

    bat["AssocArrays"] = "Is this working yet?";
    printf("%s - Yes, it is working\n", bat["AssocArrays"]);
    printf("%s\n", b["test"]);

    newarray = array("a key" => 123, 987 => array(653 => -100, "a meta key"));
    printf("%s : %d\n", newarray[987][654], newarray["a key"]);

    myArray = array_fill(-7, 10, "bananas");
    secondMyArray = array_flip(myArray);
    printf("%d\n", count(myArray));
    printf("%d\n", count(secondMyArray));

    myArray[-2] = 28;

    if (array_key_exists(8, myArray))
        printf("Key 8 with value '%s' exists!\n", myArray[8]);

    printf("%d\n", array_key_first(myArray));
    printf("%s\n", array_key_last(secondMyArray));

    keysArray = array_keys(myArray);
    printf("Size of keysArray is %d\n", count(keysArray));
    printf("%d\n", myArray[-2]);
    printf("%d %s %s %s %s %s %s %s %s %s %s\n", myArray[-2], myArray[0], myArray[-7], myArray[1], myArray[2], myArray[3], myArray[4], myArray[5], myArray[6], myArray[7], myArray[8]);

    array_push(secondMyArray, 5, "pop goes the weasel");
    char *popcorn = array_pop(secondMyArray);
    printf("%s\n", popcorn);
    printf("test %d\n", array_pop(secondMyArray));

    printf("Key %d in myArray contains \"bananas\"\n", array_search("bananas", myArray));
    printf("Key %s is gone\n", array_shift(myArray));
    printf("Key %d in myArray contains \"bananas\"\n", array_search("bananas", myArray));

    slicedarray = array_slice(a, 1);
    printf("Size of slicedarray is %d\n", count(slicedarray));
    printf("%s\n", array_pop(slicedarray));
    printf("Size of slicedarray is %d\n", count(slicedarray));
    printf("%s\n", array_pop(slicedarray));
    printf("Size of slicedarray is %d\n", count(slicedarray));

    printf("Sum of newarray is %d\n", array_sum(newarray));
    newarray["sumkey"] = (1000 - 123);
    printf("Sum of newarray is %d\n", array_sum(newarray));

    array_unshift(newarray, 1, 10, 100);
    printf("Sum of newarray is %d\n", array_sum(newarray));
    printf("%d\n", array_shift(newarray));
    printf("Sum of newarray is %d\n", array_sum(newarray));
    printf("%d\n", array_shift(newarray));
    printf("Sum of newarray is %d\n", array_sum(newarray));
    printf("%d\n", array_shift(newarray));
    printf("Sum of newarray is %d\n", array_sum(newarray));

    valuesArray = array_values(myArray);
    printf("valuesArray has %d values\n", count(valuesArray));
    printf("The last value is %d\n", valuesArray[9]);
    printf("We can also find the last value using end to get %d\n", end(valuesArray));

    printf("It is %s that the value '28' exists in valuesArray\n", in_array(28, valuesArray) ? "TRUE" : "FALSE");
    printf("However, is %s that the value 'twenty-eight' exists in valuesArray\n", in_array("twenty-eight", valuesArray) ? "TRUE" : "FALSE");
    printf("It is %s that the value '13' exists in valuesArray\n", in_array(13, valuesArray) ? "TRUE" : "FALSE");
    printf("However, is %s that the value 'bananas' exists in valuesArray\n", in_array("bananas", valuesArray) ? "TRUE" : "FALSE");

    theranges = range(4, 14, 2);
    printf("%d %d %d %d %d %d - count %d, sum %d\n", theranges[0], theranges[1], theranges[2], theranges[3], theranges[4], theranges[5], count(theranges), array_sum(theranges));
    printf("The first element of theranges is %d\n", reset(theranges));
    printf("ANd the first of a is %s\n", reset(a));
    printf("ANd the first of a is %s\n", reset(a));

    printf("Using sizeof is equal to count, so size a is %d\n", sizeof(a));

    asort(theranges);
    theranges["seventeen"] = -1;
    asort(theranges, 1, 0);
    asort(theranges, 1, 1);
    asort(theranges, 0, 1);
    asort(theranges, 0, 0);

    theranges[] = "a string!";
    asort(theranges);
    theranges[] = "a string!b";
    asort(theranges);
    theranges[] = "a string!a";
    asort(theranges);
    theranges[] = "a string!c";
    asort(theranges);





    a["xaver"] = "ravex";
    a["xavera"] = 17;
    a["xaveron"] = "ravexon";

    b["dimension"] = a;

    
    printf("%s\n", a["xaver"]);
    printf("%s\n", a["xaveron"]);

    printf("%d\n", a["xavera"]);
    printf("%d\n", a["xavera"] - 10);
    b["test"] = "first test";
    printf("%s\n", b["test"]);


    printf("%s\n", b["dimension"]["xaver"]);

    bat["AssocArrays"] = "Is this working yet?";
    printf("%s - Yes, it is working\n", bat["AssocArrays"]);

    a[17] = bat;
    a[18] = "Division stuff...";
    a[34] = "Weird keys";
    printf("%s\n", b["dimension"][a["xavera"] + a["xavera"]]);
    printf("%s - More Meta\n", b["dimension"][b["dimension"]["xavera"]]["AssocArrays"]);
    printf("%s\n", b["dimension"][a["xavera"] + (17 / a["xavera"])]);

    return 0;
}
