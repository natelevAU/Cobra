#include <stdio.h>

int main(void) {

    empty_array = array();
    empty_array_flipped = array_flip(empty_array);
    array_print(empty_array);
    array_print(empty_array_flipped);
    printf("\n");

    first_array = [0, 1, 2, 3, 4];
    first_array_flipped = array_flip(first_array);
    array_print(first_array);
    array_print(first_array_flipped);
    printf("\n");

    second_array = [4, 3, 2, 1, 0];
    second_array_flipped = array_flip(second_array);
    array_print(second_array);
    array_print(second_array_flipped);
    printf("\n");

    third_array = array("xaver" => "ravex", "xavera" => "ravexa", "xaveron" => "ravexon");
    third_array_flipped = array_flip(third_array);
    array_print(third_array);
    array_print(third_array_flipped);
    printf("\n");

    fourth_array = array("string", -20 => "negative twenty", "twenty-one" => 21, 60 => -100, "flip");
    fourth_array_flipped = array_flip(fourth_array);
    array_print(fourth_array);
    array_print(fourth_array_flipped);
    printf("\n");

    return 0;
}
