#include <stdio.h>

int main(void) {

    first_array = [0, 1, -1 => 2, "three", "four", "minustwo" => "five"];

    array_print(first_array);
    printf("Pop %s\n", array_pop(first_array));
    array_print(first_array);
    printf("Pop %s\n", array_pop(first_array));
    array_print(first_array);
    printf("Pop %s\n", array_pop(first_array));
    array_print(first_array);
    printf("Pop %d\n", array_pop(first_array));
    array_print(first_array);
    printf("Pop %d\n", array_pop(first_array));
    array_print(first_array);
    printf("Pop %d\n", array_pop(first_array));
    array_print(first_array);

    return 0;
}
