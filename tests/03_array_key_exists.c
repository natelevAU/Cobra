#include <stdio.h>

int main(void) {

    first_array = [];
    printf("%d %d\n", array_key_exists(17, first_array), array_key_exists("string", first_array));

    first_array[17] = "something";
    printf("%d %d\n", array_key_exists(17, first_array), array_key_exists("string", first_array));

    first_array["string"] = -100;
    printf("%d %d\n", array_key_exists(17, first_array), array_key_exists("string", first_array));

    second_array = array_flip(first_array);
    printf("%d %d ", array_key_exists(17, second_array), array_key_exists("string", second_array));
    printf("%d %d\n", array_key_exists(-100, second_array), array_key_exists("something", second_array));

    return 0;
}
