#include "array.h"

#include <ctype.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>



Array newArray() {
    Array array = malloc(sizeof(struct array));
    array->type = DataTypeArray;
    array->size = 0;
    array->highestKey = INT_MAX;
    array->head = NULL;
    array->tail = NULL;
    array->current = NULL;
    return array;
}

ArrayNode newArrayNode(Key key, int keyType, Mixed element, int elementType) {
    ArrayNode arrayNode = malloc(sizeof(struct arrayNode));
    setArrayNodeKey(arrayNode, key, keyType);
    setArrayNodeElement(arrayNode, element, elementType);
    arrayNode->next = NULL;
    arrayNode->prev = NULL;
    return arrayNode;
}

void deleteArrayNode(ArrayNode arrayNode) {
    if (arrayNode == NULL)
        return;
    if (arrayNode->keyType == DataTypeLiteral)
        free(arrayNode->key);
    if (arrayNode->elementType == DataTypeLiteral)
        free(arrayNode->element);

    free(arrayNode);
}

void deleteArray(Array array) {
    if (array == NULL)
        return;
    ArrayNode currNode = array->head;
    if (currNode != NULL) {
        while (currNode->next != NULL) {
            currNode = currNode->next;
            deleteArrayNode(currNode->prev);
        }
        deleteArrayNode(currNode);
    }

    free(array);
}



Mixed arrayCurrent(Array array) {
    if (array == NULL || array->current == NULL)
        return FALSE;
    return array->current->element;
}

int arrayCurrentType(Array array) {
    if (array == NULL || array->current == NULL)
        return DataTypeInt;
    return array->current->elementType;
}

Mixed setArrayPointer(Array array, ArrayNode arrayNode) {
    array->current = arrayNode;
    return arrayCurrent(array);
}

Mixed setArrayPointerFirst(Array array) {
    if (array == NULL)
        return FALSE;
    return setArrayPointer(array, array->head);
}

Mixed setArrayPointerLast(Array array) {
    if (array == NULL)
        return FALSE;
    return setArrayPointer(array, array->tail);
}

Mixed setArrayPointerNext(Array array) {
    ArrayNode currNode;
    if (array == NULL || (currNode = array->current) == NULL)
        return FALSE;
    return setArrayPointer(array, currNode->next);
}

Mixed setArrayPointerPrev(Array array) {
    ArrayNode currNode;
    if (array == NULL || (currNode = array->current) == NULL)
        return FALSE;
    return setArrayPointer(array, currNode->prev);
}

void setArrayNodeKey(ArrayNode arrayNode, Key key, int keyType) {
    if (arrayNode == NULL)
        return;
    arrayNode->keyType = keyType;

    if (isStr(key, keyType)) {
        if (key == NULL) {
            arrayNode->key == NULL;
        } else {
            arrayNode->key = malloc(strlen(key) + 1);
            strcpy(arrayNode->key, key);
        }
    } else {
        arrayNode->key = key;
    }
}

void setArrayNodeElement(ArrayNode arrayNode, Mixed element, int elementType) {
    if (arrayNode == NULL)
        return;

    arrayNode->type = elementType;
    arrayNode->elementType = elementType;

    if (isStr(element, elementType)) {
        if (element == NULL) {
            arrayNode->element == NULL;
        } else {
            arrayNode->element = malloc(strlen(element) + 1);
            strcpy(arrayNode->element, element);
        }
    } else {
        arrayNode->element = element;
    }
}



void arrayPrint(Array array) {
    printf("[ ");

    if (array != NULL) {
        for (ArrayNode currNode = array->head; currNode != NULL; currNode = currNode->next) {
            varPrint(currNode->key, currNode->keyType);
            printf(" => ");
            varPrint(currNode->element, currNode->elementType);
            printf("%s ", currNode->next != NULL ? "," : "");
        }
    }

    printf("]");
}

void varPrint(Mixed data, int dataType) {
    switch (dataType) {
        case DataTypeArray:
            arrayPrint(data);
            break;
        case DataTypeLiteral:
        case DataTypeString:
            printf("%s", data);
            break;
        case DataTypeChar:
            printf("%c", data);
            break;
        case DataTypeInt:
            printf("%d", data);
            break;
        case DataTypeFloat:
            printf("%f", data);
            break;
        case DataTypeDouble:
            printf("%lf", data);
            break;
        default:
            printf("NULL");
    }
}

void swapArrayNodes(Array array, ArrayNode node1, ArrayNode node2) {
    if (array == NULL || node1 == NULL || node2 == NULL)
        return;

    Key key1 = node1->key;
    int keyType1 = node1->keyType;
    Mixed element1 = node1->element;
    int elementType1 = node1->elementType;
    int type1 = node1->type;

    node1->key = node2->key;
    node1->keyType = node2->keyType;
    node1->element = node2->element;
    node1->elementType = node2->elementType;
    node1->type = node2->type;

    node2->key = key1;
    node2->keyType = keyType1;
    node2->element = element1;
    node2->elementType = elementType1;
    node2->type = type1;
}

int arrayCount(Array array, int mode) {
    if (array == NULL)
        return 0;
    
    int count = array->size;

    if (mode == COUNT_RECURSIVE) {
        for (ArrayNode currNode = array->head; currNode != NULL; currNode = currNode->next) {
            if (currNode->elementType == DataTypeArray)
                count += arrayCount(currNode->element, mode);
        }
    }

    return count;
}



ArrayNode addArrayNode(Array array, ArrayNode arrayNode) {
    if (array == NULL || arrayNode == NULL)
        return;
    
    if (array->current == NULL)
        array->current = arrayNode;
    
    if (arrayNode->key == NULL && arrayNode->keyType == DataTypeUnknown) {
        // No key given - set key to increment of highest int
        arrayNode->keyType = DataTypeInt;
        if (array->highestKey == INT_MAX) {
            array->highestKey = 0;
        } else {
            array->highestKey++;
        }
        arrayNode->key = array->highestKey;
    }

    return addArrayNodeEnd(array, arrayNode);
}

ArrayNode addArrayNodeStart(Array array, ArrayNode arrayNode) {
    if (array == NULL || arrayNode == NULL)
        return NULL;
    
    if (arrayNode->keyType == DataTypeInt && (int) arrayNode->key >= 0) {
        if (array->highestKey == INT_MAX) {
            array->highestKey = arrayNode->key >= 0 ? arrayNode->key : -1;
        } else {
            array->highestKey = array->highestKey < arrayNode->key ? arrayNode->key : array->highestKey;
        }
    }

    if (array->size == 0) {
        array->tail = arrayNode;
    } else {
        arrayNode->next = array->head;
        array->head->prev = arrayNode;
    }
    
    array->head = arrayNode;
    array->size++;

    return arrayNode;
}

ArrayNode addArrayNodeEnd(Array array, ArrayNode arrayNode) {
    if (array == NULL || arrayNode == NULL)
        return NULL;

    if (arrayNode->keyType == DataTypeInt && (int) arrayNode->key >= 0) {
        if (array->highestKey == INT_MAX) {
            array->highestKey = arrayNode->key >= 0 ? arrayNode->key : -1;
        } else {
            array->highestKey = array->highestKey < arrayNode->key ? arrayNode->key : array->highestKey;
        }
    }

    if (array->size == 0) {
        array->head = arrayNode;
    } else {
        arrayNode->prev = array->tail;
        array->tail->next = arrayNode;
    }
    
    array->tail = arrayNode;
    array->size++;

    return arrayNode;
}

ArrayNode addArrayNodeAfter(Array array, ArrayNode newNode, ArrayNode arrayNode) {
    if (array == NULL || newNode == NULL)
        return NULL;
    
    if (arrayNode == NULL) {
        return addArrayNodeStart(array, newNode);
    } else
    if (arrayNode->next == NULL) {
        return addArrayNodeEnd(array, newNode);
    }

    ArrayNode nextNode = arrayNode->next;
    if (nextNode != NULL) {
        newNode->next = nextNode;
        nextNode->prev = newNode;
    }

    arrayNode->next = newNode;
    newNode->prev = arrayNode;

    array->size++;

    return newNode;
}

ArrayNode removeArrayNode(Array array, ArrayNode arrayNode) {
    if (array == NULL || arrayNode == NULL || array->size == 0)
        return NULL;

    if (arrayNode->prev != NULL) {
        ArrayNode prevNode = arrayNode->prev;
        prevNode->next = arrayNode->next;
    } else {
        array->head = arrayNode->next;
    }

    if (arrayNode->next != NULL) {
        ArrayNode nextNode = arrayNode->next;
        nextNode->prev = arrayNode->prev;
    } else {
        array->tail = arrayNode->prev;
    }

    array->size--;
    return arrayNode;
}

ArrayNode removeArrayNodeStart(Array array) {
    if (array == NULL || array->size == 0)
        return NULL;

    ArrayNode arrayNode = array->head;
    array->head = arrayNode->next;
    array->size--;

    if (array->size == 0) {
        array->tail = NULL;
    } else {
        arrayNode->next->prev = NULL;
    }

    return arrayNode;
}

ArrayNode removeArrayNodeEnd(Array array) {
    if (array == NULL || array->size == 0)
        return NULL;

    ArrayNode arrayNode = array->tail;
    array->tail = arrayNode->prev;
    array->size--;
    
    if (array->size == 0) {
        array->head = NULL;
    } else {
        arrayNode->prev->next = NULL;
    }    

    return arrayNode;
}



ArrayNode getArrayNodeByKey(Array array, Key key, DataType keyType, int initialise) {
    ArrayNode currNode = NULL;
    if (array != NULL && array->size != 0 && (key != NULL || keyType != DataTypeUnknown)) {
        currNode = array->head;
        while (currNode != NULL) {
            if (keyComparable(currNode, key, keyType) && keyCmp(currNode, key, keyType) == 0)
                break;
            currNode = currNode->next;
        }
    }

    return (initialise && currNode == NULL) ? addArrayNodeEnd(array, newArrayNode(key, keyType, NULL, NULL)) : currNode;
}

ArrayNode getArrayNodeByElement(Array array, Mixed element, DataType elementType, int initialise) {
    ArrayNode currNode = NULL;
    if (! (array == NULL || array->size == 0 || (element == NULL && elementType == DataTypeUnknown))) {
        currNode = array->head;
        while (currNode != NULL) {
            if (elementComparable(currNode, element, elementType) && elementCmp(currNode, element, elementType) == 0)
                break;
            currNode = currNode->next;
        }
    }
    
    return (initialise && currNode == NULL) ? addArrayNode(array, newArrayNode(NULL, NULL, element, elementType)) : currNode;
}

ArrayNode getArrayNodeByIndex(Array array, int index) {
    ArrayNode currNode = NULL;
    index = (index >= 0) ? index : array->size + index;
    if (! (array == NULL || index >= array->size || index < 0)) {
        currNode = array->head;
        for (int i = 0; i < index; i++) {
            currNode = currNode->next;
        }
    }
    
    return currNode;
}

ArrayNode copyArrayNode(ArrayNode arrayNode, int preserve_keys) {
    if (arrayNode == NULL)
        return NULL;

    ArrayNode newNode = newArrayNode(NULL, NULL, arrayNode->element, arrayNode->elementType);
    if (preserve_keys || arrayNode->keyType != DataTypeInt)
        setArrayNodeKey(newNode, arrayNode->key, arrayNode->keyType);
    
    return newNode;
}



int keyCmp(ArrayNode arrayNode, Key key, DataType keyType) {
    if (arrayNode == NULL || (key == NULL && keyType == DataTypeUnknown))
        return 0;

    int arrayNodeIsNum = isNum(arrayNode->key, arrayNode->keyType);
    int arrayNodeIsStr = isStr(arrayNode->key, arrayNode->keyType);

    if (isNum(key, keyType)) {
        return arrayNodeIsNum ? arrayNode->key - key : 1;
    } else
    if (isStr(key, keyType)) {
        return arrayNodeIsStr ? strcmp(arrayNode->key, key) : (arrayNodeIsNum ? -1 : 1);
    } else {
        return (arrayNodeIsNum || arrayNodeIsStr) ? -1 : 0;
    }
}

int keyComparable(ArrayNode arrayNode, Key key, DataType keyType) {
    if (arrayNode == NULL || (key == NULL && keyType == DataTypeUnknown))
        return FALSE;
    
    return !((isStr(key, keyType) ^ isStr(arrayNode->key, arrayNode->keyType)) ||
             (isNum(key, keyType) ^ isNum(arrayNode->key, arrayNode->keyType)));
}

int elementCmp(ArrayNode arrayNode, Mixed element, DataType elementType) {
    if (arrayNode == NULL || (element == NULL && elementType == DataTypeUnknown))
        return 0;

    int arrayNodeIsNum = isNum(arrayNode->element, arrayNode->elementType);
    int arrayNodeIsStr = isStr(arrayNode->element, arrayNode->elementType);

    if (isNum(element, elementType)) {
        return arrayNodeIsNum ? arrayNode->element - element : 1;
    } else
    if (isStr(element, elementType)) {
        return arrayNodeIsStr ? strcmp(arrayNode->element, element) : (arrayNodeIsNum ? -1 : 1);
    } else {
        return (arrayNodeIsNum || arrayNodeIsStr) ? -1 : 0;
    }
}

int elementComparable(ArrayNode arrayNode, Mixed element, DataType elementType) {
    if (arrayNode == NULL || (element == NULL && elementType == DataTypeUnknown))
        return FALSE;
    
    return !((isStr(element, elementType) ^ isStr(arrayNode->element, arrayNode->elementType)) ||
            (isNum(element, elementType) ^ isNum(arrayNode->element, arrayNode->elementType)));
}

int arraySort(Array array, int by_val, int direction_up) {
    if (array == NULL)
        return 0;    
    
    int direction = (direction_up) ? 1 : -1;
    int (*nodeCmp)(ArrayNode, Mixed, DataType) = (by_val) ? &elementCmp : &keyCmp;
    ArrayNode sortedNode = array->head;

    for (ArrayNode currNode; sortedNode != NULL; sortedNode = sortedNode->next) {
        currNode = sortedNode;
        for (ArrayNode prevNode = currNode->prev; prevNode != NULL; prevNode = prevNode->prev, currNode = currNode->prev) {
            Mixed val = (by_val) ? prevNode->element : prevNode->key;
            int valType = (by_val) ? prevNode->elementType : prevNode->keyType;
            int cmp = direction * nodeCmp(currNode, val, valType);
            if (cmp < 0) {
                swapArrayNodes(array, prevNode, currNode);
            } else {
                break;
            }
        }
    }
    ArrayNode currNode;
    if (array->tail->elementType == DataTypeString || array->tail->elementType == DataTypeLiteral)
        printf("%s\n", array->tail->element);

    return TRUE; // TODO Finish asort
}



int isStrNum(char *str) {
    int len;
    if (str == NULL || (len = strlen(str)) == 0) {
        return FALSE;
    }

    int hasDecimal = FALSE;
    for (int i = 0; i < len; i++) {
        if (isdigit(str[i]))
            continue;
        if (str[i] == '.' && hasDecimal == FALSE) {
            hasDecimal = TRUE;
            continue;
        }
        return FALSE;
    }
    return TRUE;
}

int isNum(void *data, int dataType) {
    return  (dataType == DataTypeInt) ||
            (dataType == DataTypeDouble) ||
            ((dataType == DataTypeLiteral) && isStrNum(data)) ||
            ((dataType == DataTypeString) && isStrNum(data))
            ;
}

int isStr(void *data, int dataType) {
    return  (dataType == DataTypeLiteral) ||
            (dataType == DataTypeString)
            ;
}

int isKey(void *data, int dataType) {
    return isStr(data, dataType) || isNum(data, dataType);
}



int calcBitSize(Object value) {
    uint64_t mask = 0x1 << ((4 * sizeof(Object)) - 1);
    int bitSize = sizeof(Object) * 8;
    uint64_t valueBin = (uint64_t) value;

    for (int i = 0; i < sizeof(Object) * 4; i++, bitSize--) {
        if (mask & valueBin)
            break;
        mask = mask >> 1;
    }

    return bitSize;
}

int calcIsNum(Object value, int valueType) {
    int bitSize = calcBitSize(value) / 8;
    if (bitSize != sizeof(void *) && bitSize - 1 != sizeof(void *))
        return TRUE;
    return FALSE;
}

int calcIsArray(void *value, int valueType) {
    return valueType == DataTypeArray;
}

int calcNumType(Object value, int valueType) {
    if (valueType != DataTypeUnknown)
        return valueType;
    
    int valueSize = calcBitSize(value);
    if (valueSize < sizeof(float) - 9)
        return DataTypeInt;
    if (valueSize == (sizeof(int) * 8) && valueSize == (sizeof(float) * 8))
        return (calcBitSize(INT_MAX & (int) value) < (sizeof(float) * 8) - 9) ? DataTypeInt : DataTypeFloat;
    if (valueSize < (sizeof(double) * 8) - 12)
        return DataTypeFloat;
    if (valueSize <= (sizeof(double) * 8))
        return DataTypeDouble;
    
    return valueType;
}

int calcPointerType(void *value, int valueType) {
    return valueType;
}

int calcKeyType(Object value, int valueType) {
    if (valueType != DataTypeUnknown)
        return valueType;
    
    if (calcIsNum(value, valueType)) {
        return calcNumType(value, valueType);
    } else {
        return calcPointerType(value, valueType);
    }
}

int calcMixedType(Object value, int valueType) {
    valueType = calcIsArray(value, valueType) ? DataTypeArray : calcKeyType(value, valueType);
    return valueType;
}
