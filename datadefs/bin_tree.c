#include "bin_tree.h"

BinTreeNode newBinTreeNode(const char* name, void* value)
{
    BinTreeNode new = malloc(sizeof(struct binTreeNode));
    new->left = NULL;
    new->right = NULL;
    new->child = NULL;
    new->name = malloc((strlen(name) + 1) * sizeof(char*));
    strcpy(new->name, name);
    binTreeNodeSetValue(new, value);
    new->id = 0;
    return new;
}

BinTree newBinTree()
{
    BinTree newTree = malloc(sizeof(struct binTree));
    newTree->root = NULL;
    newTree->numNodes = 0;
    return newTree;
}

BinTree newBinTreeID(void* id)
{
    BinTree new = newBinTree();
    new->id = id;
    return new;
}

void binTreeNodeSetValue(BinTreeNode node, void* value)
{
    node->value = value;
}

void binTreeNodeSetId(BinTreeNode node, int id)
{
    node->id = (int) id;
}

void deleteBinTreeNode(BinTreeNode node) {
    if (node == NULL) return;
    deleteBinTree(node->child);
    deleteBinTreeNode(node->left);
    deleteBinTreeNode(node->right);
    if (node->value) free(node->value);
    free(node->name);
    free(node);
}

void deleteBinTree(BinTree tree) {
    if (tree == NULL) return;
    deleteBinTreeNode(tree->root);
    free(tree);
}



BinTreeNode binTreeSearch(BinTree tree, const char* name)
{
    if (tree == NULL) return NULL; // tree is NULL, return NULL
    BinTreeNode node;
    BinTreeNode nextNode = tree->root;
    if (! nextNode) return NULL; // empty tree, return NULL
    int cmp;
    while (nextNode) {
        node = nextNode;
        cmp = strcmp(node->name, name);
        if (cmp == 0) break;
        nextNode = cmp > 0 ? node->left : node->right;
    }
    return node;
}

BinTreeNode binTreeGet(BinTree tree, const char* name)
{
    BinTreeNode treeNode = binTreeSearch(tree, name);
    if (treeNode == NULL || strcmp(treeNode->name, name) != 0) {
        treeNode = binTreeAddNode(tree, name, NULL);
    }
    return treeNode;
}

BinTreeNode binTreeAddNode(BinTree tree, const char* name, void* value)
{
    if (tree->numNodes == 0) { // empty tree, add root node
        tree->root = newBinTreeNode(name, value);
        tree->numNodes++;
        return tree->root;
    }
    BinTreeNode node = binTreeSearch(tree, name);
    int cmp = strcmp(node->name, name);
    if (cmp == 0) {
        binTreeNodeSetValue(node, value);
        return node;
    }
    tree->numNodes++;
    BinTreeNode new = newBinTreeNode(name, value);
    if (cmp > 0) node->left = new;
    if (cmp < 0) node->right = new;

    // TODO Balance tree
    return new;
}

BinTree binNodeAddTree(BinTreeNode node) {
    if (node->child) {
        deleteBinTree(node->child);
    }
    node->child = newBinTree();
    return node->child;
}
