#include "token.h"


TokenList newTokenList() {
    TokenList tokenList = malloc(sizeof(struct tokenList));
    tokenList->head = NULL;
    tokenList->tail = NULL;
    tokenList->len = 1;
    return tokenList;
}

TokenNode newTokenNode(Token token, char *whitespace, int *whitespacePos) {
    TokenNode tokenNode = malloc(sizeof(struct tokenNode));
    if (whitespace != NULL) {
        int whitespaceLen;
        if (whitespacePos == NULL || *whitespacePos == 0) {
            whitespaceLen = strlen(whitespace);
        } else {
            whitespaceLen = *whitespacePos;
            *whitespacePos = 0;
        }
        token->len += whitespaceLen;
        token->preWhitespace = malloc(whitespaceLen + 1);
        strcpy(token->preWhitespace, whitespace);
        if (whitespacePos != NULL) {
            memset(whitespace, '\0', whitespaceLen);
        }
    }
    tokenNode->token = token;
    tokenNode->next = NULL;
    return tokenNode;
}

void deleteTokenNode(TokenNode tokenNode) {
    if (tokenNode == NULL)
        return;
    if (tokenNode->token != NULL)
        deleteToken(tokenNode->token);
    free(tokenNode);
}

void deleteTokenList(TokenList tokenList) {
    if (tokenList == NULL)
        return;
    TokenNode currNode = tokenList->head;
    for (TokenNode nextNode; currNode != NULL; currNode = nextNode) {
        nextNode = currNode->next;
        deleteTokenNode(currNode);
    }
    free(tokenList);
}



Token newToken() {
    Token token = malloc(sizeof(struct token));
    token->value = NULL;
    token->preWhitespace = NULL;
    token->type = 0;
    token->len = 0;
    return token;
}

Token newIdentifierToken(char *identifier) {
    Token token = newToken();
    token->value = malloc(strlen(identifier) + 1);
    strcpy(token->value, identifier);
    token->type = TokenIdentifier;
    token->len = strlen(identifier);
    return token;
}

Token newNumberToken(char *number) {
    Token token = newToken();
    token->value = malloc(strlen(number) + 1);
    strcpy(token->value, number);
    token->type = TokenNumber;
    token->len = strlen(number);
    return token;
}

Token newLiteralToken(char *literal) {
    Token token = newToken();
    token->value = malloc(strlen(literal) + 1);
    strcpy(token->value, literal);
    token->type = TokenLiteral;
    token->len = strlen(literal);
    return token;
}

Token newPunctuatorToken(char *punctuator) {
    Token token = newToken();
    token->value = malloc(strlen(punctuator) + 1);
    strcpy(token->value, punctuator);
    token->type = TokenPunctuator;
    token->len = strlen(punctuator);
    return token;
}

Token newOtherToken(char c) {
    Token token = newToken();
    token->value = malloc(sizeof(2));
    token->value[0] = c;
    token->value[1] = '\0';
    token->type = TokenOther;
    token->len = 1;
    return token;
}

void deleteToken(Token token) {
    if (token == NULL)
        return;
    if (token->value != NULL)
        free(token->value);
    if (token->preWhitespace != NULL)
        free(token->preWhitespace);
    free(token);
}



void addToken(TokenList tokenList, Token token, char *whitespace, int *whitespacePos) {
    TokenNode newNode = newTokenNode(token, whitespace, whitespacePos);
    if (tokenList->head == NULL) {
        tokenList->head = newNode;
    } else {
        tokenList->tail->next = newNode;
    }
    tokenList->tail = newNode;
}

TokenNode addTokenAfterNode(TokenNode tokenNode, Token token, char *whitespace, int *whitespacePos) {
    if (tokenNode == NULL || token == NULL)
        return;
    
    TokenNode newNode = newTokenNode(token, whitespace, whitespacePos);
    newNode->next = tokenNode->next;
    tokenNode->next = newNode;

    if (whitespace == NULL && newNode->next != NULL) {
        char* temp = newNode->token->preWhitespace;
        newNode->token->preWhitespace = newNode->next->token->preWhitespace;
        newNode->next->token->preWhitespace = temp;
    }

    return newNode;
}

void swapTokenNodes(TokenNode prevToken1, TokenNode prevToken2) {
    TokenNode token1 = prevToken1->next;
    TokenNode nextToken1 = token1->next;
    TokenNode token2 = prevToken2->next;
    TokenNode nextToken2 = token2->next;

    prevToken1->next = token2;
    token2->next = nextToken1;
    prevToken2->next = token1;
    token1->next = nextToken2;

    char *temp = token1->token->preWhitespace;
    token1->token->preWhitespace = token2->token->preWhitespace;
    token2->token->preWhitespace = temp;
}



void calcTokenListLen(TokenList tokenList) {
    if (tokenList == NULL) return;
    tokenList->len = 1;
    for (TokenNode currNode = tokenList->head; currNode != NULL; currNode = currNode->next) {
        tokenList->len += calcTokenLen(currNode->token);
    }
}

int calcTokenLen(Token token) {
    if (token == NULL || token->type == NULL) return 0;
    token->len = 0;
    if (token->value != NULL)
        token->len += strlen(token->value);
    if (token->preWhitespace != NULL)
        token->len += strlen(token->preWhitespace);
    return token->len;
}

char *tokenListToString(TokenList tokenList) {
    calcTokenListLen(tokenList);
    char *string = malloc(tokenList->len + 1);
    memset(string, '\0', tokenList->len + 1);
    if (tokenList == NULL || tokenList->head == NULL)
        return string;
    TokenNode currNode = tokenList->head;
    while (currNode != NULL) {
        printToken(currNode->token, string);
        currNode = currNode->next;
    }
    strcat(string, "\n");
    return string;
}

void printToken(Token token, char *string) {
    if (token == NULL || token->value == NULL || token->len == NULL) return;
    if (token->preWhitespace != NULL) {
        strcat(string, token->preWhitespace);
    }
    strcat(string, token->value);
}



void setTokenValue(Token token, char *newValue) {
    if (token == NULL) return;
    int newValueLen = strlen(newValue);

    if (token->value != NULL) {
        free(token->value);
    }
    token->value = malloc(newValueLen + 1);

    strcpy(token->value, newValue);
}

