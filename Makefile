BIN = cobra

PICOC_OBJS = picoc/*.c picoc/cstdlib/*.c
# EXCLUDE picoc/platform/platform_unix.c picoc/platform/library_unix.c

OBJS = *.c $(PICOC_OBJS) preprocessor/*.c lex/*.c datadefs/*.c


CC = gcc
CFLAGS=-Wall -pedantic -g -DUNIX_HOST -DVER=\"2.1\" \
	   -I ./ \
	   -I ./picoc \
	   -I ./include \
	   -I ./datadefs
LIBS=-lm -lreadline


all: $(BIN)

$(BIN) : $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJS) $(LIBS) -o $(BIN)

clean:
	rm -rf *.o $(BIN)
